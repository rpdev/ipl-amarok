VERSION=$(shell sh extract_version.sh)
dist: translations
	echo $(VERSION)
	mkdir intelligent_playlist
	cp README intelligent_playlist
	cp COPYING intelligent_playlist
	cp AUTHORS intelligent_playlist
	cp script.spec intelligent_playlist
	cp *.js intelligent_playlist
	cp *.ui intelligent_playlist
	mkdir intelligent_playlist/translations
	cp translations/*.qm intelligent_playlist/translations
	tar -cjf intelligent_playlist-$(VERSION).amarokscript.tar.bz2 intelligent_playlist
	rm -rf intelligent_playlist
.PHONY: dist

translations:
	make -C translations
.PHONY: translations
