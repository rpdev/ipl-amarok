/*
 *    Copyright (C) 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

Importer.loadQtBinding( "qt.core" );
Importer.loadQtBinding( "qt.gui" );

//  PUBLIC

/*
 * The number of samples to reduce each moodbar to.
 * Each sample contains a RGB value which is simply
 * the average value taken from the appropriate range
 * from the source moodbar.
 */
MoodbarManager.SampleCount = 10;

/**
 * Number of mood vectors to hold in cache, relative to the TTL value for tracks.
 * The TTL is chosen to match the (average) number of tracks in the Playlist.
 * This value is a multiplier. Basically, the idea is:
 * Keep only the track's vectors that really are in the playlist and thus
 * will be needed regularly.
 */
MoodbarManager.CacheSizeMultiplier = 2;

/*
 * Basically, this is a reimplementation of Amaroks Moodbar manager.
 *
 * The manager is an interface to easily get access to the moodbars
 * given a certain URL.
 *
 * Additionally, we can fit the data to be more, what we need
 * i.e. in contrary to Amarok we don't need nice graphics but rather
 * a short sequence of numbers for comparison.
 *
 * Beneath, this class is responsible for caching the read mood data.
 * Otherwise, the number of disk reads would render the plugin
 * unusable.
 */
function MoodbarManager( plugin )
{
    this.m_plugin = plugin;
    
    this.m_cache = new Array();
    this.m_table = new QTableWidget( this );
    
    this.m_table.resize( 500, 400 );
    this.m_table.showGrid = false;
    this.m_table.columnCount = MoodbarManager.SampleCount + 3;
    var headerLabels = new Array( qsTranslate( "moodbarmanager", "URL" ),
                                  qsTranslate( "moodbarmanager", "Age" ),
                                  qsTranslate( "moodbarmanager", "Continuity" ) );
    for ( var i = 0; i < MoodbarManager.SampleCount; i++ )
    {
        this.m_table.horizontalHeader().setResizeMode( i + 3, QHeaderView.Fixed );
        this.m_table.setColumnWidth( i + 3, 10 );
        headerLabels.push( "" );
    }
    this.m_table.horizontalHeader().setResizeMode( 0, QHeaderView.Interactive );
    this.m_table.horizontalHeader().setResizeMode( 1, QHeaderView.Interactive );
    this.m_table.horizontalHeader().setResizeMode( 2, QHeaderView.Interactive );
    this.m_table.setHorizontalHeaderLabels( headerLabels );

    this.m_timer = new QTimer( this );
    this.m_timer.interval = 1000;
    this.m_timer['timeout()'].connect( this, this.updateGui );
    this.m_timer.start();
}

/*
 * Compare two tracks using their moodbars.
 * The tracks are passed in as instances of the Track class.
 */
MoodbarManager.prototype.compareTracks = function( track1, track2 )
{
    //TODO: Fix processing of moodbars!
    return 0.0;
    if ( ! ( track1.isLocalFile() && track2.isLocalFile() ) )
    {
        return 0.0;
    }
    
    var v1 = this.findVector( track1.toFileName() );
    var v2 = this.findVector( track2.toFileName() );

    if ( v1 == null || v2 == null )
    {
        return 0.0;
    }
    
    var s1 = this.compareVectors( this.sortVectorByHue( v1 ), this.sortVectorByHue( v2 ) );
    var s2 = this.compareContinuity( v1, v2 );

    return ( s1 + s2 ) / 2.0;
}

/*
 * Show the moodbar contents for debugging.
 */
MoodbarManager.prototype.show = function()
{
    this.m_table.show();
    this.updateGui();
}




// PRIVATE

/*
 * Get the moodfile path for a given track URL.
 * Amarok itself expects the moodfiles to be stored beneath the
 * tracks, replacing the file extension with ".mood" and preceeded by
 * a dot (i.e. the file is invisible in *NIX systems).
 */
MoodbarManager.prototype.moodfile = function( trackUrl )
{
    var parts = trackUrl.split( '.' );
    parts.pop();
    parts.push( "mood" );

    var moodPath = parts.join( "." );

    //now prepend the filename with .
    var fileInfo = new QFileInfo( moodPath );
    var fileName = fileInfo.fileName();
    
    return moodPath.replace( fileName, "." + fileName );
}

/*
 * Returns a QByteArray holding the (raw) data contained in file.
 */
MoodbarManager.prototype.loadRawData = function( file )
{
    var f = new QFile( file );
    if ( !( f.open( new QIODevice.OpenMode(QIODevice.ReadOnly) ) ) )
    {
        return null;
    }
    var result = f.readAll();
    f.close();
    return result;
}

/*
 * Create a vector ready for comparison from raw input data.
 * "data" is a QByteArray instance as returned by loadRawData.
 * If either data is null or invalid, null is returned, otherwise
 * a vector of MoodbarManager.SampleCount RGB-triples.
 */
MoodbarManager.prototype.createVector = function( data )
{
    Amarok.debug( "MoodbarManager: Processing input data." );
    if ( data == null ) // No data available, thus, no output vector.
    {
        return null;
    }
    if ( data.size() % 3 != 0 ) // The input seems to be corrupt. Discard as well!
    {
        Amarok.debug( "MoodbarManager: Warning: Input data seems to be corrupted!" );
        return null;
    }
    
    var result = new Array();
    var dataSampleCount = data.size() / 3;
    
    for ( var sample = 0; sample < MoodbarManager.SampleCount; sample++ )
    {
        var inputSamples = 0;
        var r = 0, g = 0, b = 0;
        var len = parseInt( dataSampleCount / MoodbarManager.SampleCount, 10 );
        var off = len * sample;
        for ( var s = off; s < off + len; s++ )
        {
            inputSamples++;
            r += data.at( s * 3 + 0 );
            g += data.at( s * 3 + 1 );
            b += data.at( s * 3 + 2 );
        }
        if ( inputSamples == 0 ) // this should not happen!
        {
            return null; 
        }
        var entry = new Array();
        r = r / inputSamples;
        g = g / inputSamples;
        b = b / inputSamples;
        entry.push(
            parseInt( r, 10 ),
            parseInt( g, 10 ),
            parseInt( b, 10 )
        );
        result.push( entry );
    }

    for ( var i = 0; i < result.length; i++ )
    {
        for ( j = 0; j < result[ i ].length; j++ )
        {
            if ( isNaN( result[ i ][ j ] ) )
            {
                result[ i ][ j ] = -128;
            }
        }
    }
    var s = "";
    for ( var i = 0; i < result.length; i++ )
    {
        s = s + " " + result[ i ].join( "|" );
    }
    Amarok.debug( "Moodbar: " + s );
    return result;
}

/*
 * Compares two moodbar vectors.
 * Returns a normalized similarity value, where 0.0 means not similar and
 * 1.0 very similar.
 */
MoodbarManager.prototype.compareVectors = function( vector1, vector2 )
{
    if ( vector1 == null || vector2 == null )
    {
        return 0.0;
    }
    var cumSum = 0.0;
    for ( var i = 0; i < MoodbarManager.SampleCount; i++ )
    {
        var dr = vector1[ i ][ 0 ] - vector2[ i ][ 0 ];
        var dg = vector1[ i ][ 1 ] - vector2[ i ][ 1 ];
        var db = vector1[ i ][ 2 ] - vector2[ i ][ 2 ];
        var d = Math.sqrt( dr * dr + dg * dg + db * db );
        cumSum += d;
    }
    var result = 1.0 - cumSum / ( MoodbarManager.SampleCount * Math.sqrt( 3 * 255 * 255 ) );
    Amarok.debug( "MoodbarManager: Calculated vector similarity: " + result );
    if ( isNaN( result ) )
    {
        return 0.0;
    }
    return Math.max( 0.0, Math.min( 1.0, result ) );
}

/*
 * Compares the continuity of two moodbar vectors.
 * Outcome is between 0.0 and 1.0, where 1.0 means very similar.
 */
MoodbarManager.prototype.compareContinuity = function( vector1, vector2 )
{
    var result = Math.max( this.vectorContinuity( vector1 ) - this.vectorContinuity( vector2 ), 0.0 );
    Amarok.debug( "MoodbarManager: Continuity: " + result );
    return result;
}

/*
 * Calculates the continuity of a moodbar vector.
 * The value is between 1.0 and 0.0, where 1.0 means high
 * continuity.
 */
MoodbarManager.prototype.vectorContinuity = function( vec )
{
    if ( vec == null || MoodbarManager.SampleCount <= 1 )
    {
        return 0.0;
    }
    
    var c = 0.0;
    for ( var i = 0; i < MoodbarManager.SampleCount - 1; i++ )
    {
        var dr = ( vec[ i ][ 0 ] - vec[ i + 1 ][ 0 ] ) / 255;
        var dg = ( vec[ i ][ 1 ] - vec[ i + 1 ][ 1 ] ) / 255;
        var db = ( vec[ i ][ 2 ] - vec[ i + 1 ][ 2 ] ) / 255;

        c += Math.sqrt( dr * dr + dg * dg + db * db );
    }

    var result = 1.0 - c / parseFloat( MoodbarManager.SampleCount - 1 );
    if ( isNaN( result ) )
    {
        return 0.0;
    } else
    {
        return result;
    }
}

/*
 * Convert RGB to HSV.
 *
 * Converts a color in RGB format ( "red-green-blue" ) to
 * HSV ( "hue-saturation-value" ). Returns a 3-elements sized array
 * with values between 0 and 255.
 */
MoodbarManager.prototype.rgbToHsv = function( r, g, b )
{
    var max = Math.max( r + 128, g + 128, b + 128 );
    var min = Math.min( r + 128, g + 128, b + 128 );

    var h = 0;
    if ( r == g && g == b )
    {
        // nothing to do. Hue is zero
    } else if ( max == r )
    {
        h = 60 * ( 0 + ( g - b ) / ( max - min ) );
    } else if ( max == g )
    {
        h = 60 * ( 2 + ( b - r ) / ( max - min ) );
    } else // if ( max == b )
    {
        h = 60 * ( 4 + ( r - g ) / ( max - min ) );
    }
    if ( h < 0 )
    {
        h = h + 360;
    }
    h = parseInt( ( parseFloat( h ) / 360.0 ) * 255, 10 );

    var s = 0;
    if ( max > 0 )
    {
        s = ( max - min ) / min;
    }

    v = max;

    return new Array( h, s, v );
}

/*
 * Returns a sorted version of vector, sorted by the hue of
 * the distinct segments.
 */
MoodbarManager.prototype.sortVectorByHue = function( vector )
{
    if ( vector == null )
    {
        return vector;
    }
    var hv = new Array();
    var result = new Array();
    for ( var i = 0; i < MoodbarManager.SampleCount; i++ )
    {
        hv.push( this.rgbToHsv( vector[ i ][ 0 ], vector[ i ][ 1 ], vector[ i ][ 2 ] ) );
        result.push( vector[ i ] );
    }

    for ( var i = 0; i < MoodbarManager.SampleCount - 1; i++ )
    {
        for ( var j = i + 1; j < MoodbarManager.SampleCount; j++ )
        {
            if ( hv[ i ][ 0 ] > hv[ j ][ 0 ] )
            {
                var tmp = hv[ i ];
                hv[ i ] = hv[ j ];
                hv[ j ] = tmp;

                tmp = result[ i ];
                result[ i ] = result[ j ];
                result[ j ] = tmp;
            }
        }
    }

    return result;
}

/*
 * Load a file's moodbar into the cache.
 */
MoodbarManager.prototype.cacheFile = function( trackUrl )
{
    var moodfile = this.moodfile( trackUrl );
    var data = this.loadRawData( moodfile );
    var vector = this.createVector( data );

    var entry = new Array();
    entry.push( trackUrl );
    entry.push( vector );
    entry.push( 0 );

    this.m_cache.push( entry );
    
    return vector;
}

/*
 * Returns the vector for a given URL from the cache or null, if not
 * found or could not be created.
 */
MoodbarManager.prototype.findVector = function( trackUrl )
{
    this.tick();
    for ( var i = 0; i < this.m_cache.length; i++ )
    {
        if ( this.m_cache[ i ][ 0 ] == trackUrl )
        {
            this.m_cache[ i ][ 2 ] = 0;
            return this.m_cache[ i ][ 1 ];
        }
    }
    while ( this.m_cache.length >= this.m_plugin.configuration.cacheMaxTTL() * MoodbarManager.CacheSizeMultiplier )
    {
        this.removeCacheEntry();
    }
    return this.cacheFile( trackUrl );
}

/*
 * Update the debug view to the current cache contents.
 */
MoodbarManager.prototype.updateGui = function()
{
    if ( !( this.m_table.visible ) )
    {
        return;
    }
    
    this.m_table.setWindowTitle(
        qsTranslate( "moodbarmanager", "Moodbar Cache - %1 / %2" ).arg(
        this.m_cache.length ).arg(
        (this.m_plugin.configuration.cacheMaxTTL() * MoodbarManager.CacheSizeMultiplier) )
    );
    this.m_table.rowCount = this.m_cache.length;

    for ( var i = 0; i < this.m_cache.length; i++ )
    {
        var u = this.m_cache[ i ][ 0 ];
        var a = this.m_cache[ i ][ 2 ];
        var v = this.m_cache[ i ][ 1 ];
        var c = this.vectorContinuity( v );
        
        var url = new QTableWidgetItem( "" + u );
        var age = new QTableWidgetItem( "" + a );
        var cty = new QTableWidgetItem( "" + c );
        if ( v == null )
        {
            var font = new QFont();
            font.setItalic( true );
            url.setFont( font );
        }
        this.m_table.setItem( i, 0, url );
        this.m_table.setItem( i, 1, age );
        this.m_table.setItem( i, 2, cty );
        if ( v != null )
        {
            for ( var c = 0; c < MoodbarManager.SampleCount; c++ )
            {
                var it = new QTableWidgetItem();
                var brush = new QBrush( Qt.SolidPattern );
                brush.setColor( new QColor( v[ c ][ 0 ] + 128, v[ c ][ 1 ] + 128, v[ c ][ 2 ] + 128 ) );
                it.setBackground( brush );
                this.m_table.setItem( i, c + 3, it );
            }
        } else
        {
            for ( var c = 0; c < MoodbarManager.SampleCount; c++ )
            {
                var it = new QTableWidgetItem();
                this.m_table.setItem( i, c + 3, it );
            }
        }
    }
}

/*
 * Increase age of each cache entry.
 */
MoodbarManager.prototype.tick = function()
{
    for ( var i = 0; i < this.m_cache.length; i++ )
    {
        this.m_cache[ i ][ 2 ] = this.m_cache[ i ][ 2 ] + 1;
    }
}

/*
 * Remove the oldest cache entry.
 */
MoodbarManager.prototype.removeCacheEntry = function()
{
    var selected = -1;
    var selectedAge = 0;
    for ( var i = 0; i < this.m_cache.length; i++ )
    {
        if ( selected == -1 || this.m_cache[ i ][ 2 ] > selectedAge )
        {
            selected = i;
            selectedAge = this.m_cache[ i ][ 2 ];
        }
    }
    if ( selected >= 0 )
    {
        this.m_cache.splice( selected, 1 );
    }
}
