/*
 *    Copyright (C) 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

Importer.loadQtBinding( "qt.core" );
Importer.loadQtBinding( "qt.gui" );

/*
 * Create a new TrackCache object.
 * The track cache is used to store a set of "good" tracks. This
 * is useful to keep the impact of the calculation as little as
 * possible. Using the cache allows the algorithm to store tracks with
 * good similarity values for later runs (when maybe only bad tracks has been
 * found in a run).
 * Theoretically, the bigger the cache the better the resulting playlist will
 * be. However, big caches not only consume more memory, but also access times
 * will go down. Thus, a good balance in cache size must be found (which is
 * however dependent on the used hardware).
 */
function TrackCache( plugin )
{
    this.m_plugin = plugin;

    this.m_entries = new Array();

    this.m_table = new QTableWidget( this );
    this.m_table.resize( 400, 400 );

    this.m_timer = new QTimer( this );
    this.m_timer.interval = 1000;
    this.m_timer['timeout()'].connect( this, this.updateGui );
    this.m_timer.start();
    
}

/*
 * Returns the maximum cache size.
 * This is a shorthand to obtain the maximum cache size from the configuration
 * object.
 */
TrackCache.prototype.cacheSize = function( actualSize )
{
    return this.m_plugin.configuration.cacheSize();
}

/*
 * Inserts a track (or better a track cache object).
 * This will append the new entry, if either the cache has empty slots or
 * if there are tracks with lower similarity values.
 */
TrackCache.prototype.insertTrack = function( entry )
{
    for ( var i = 0; i < this.m_entries.length; i++ ) {
        if ( entry.similarity() >= this.m_entries[ i ].similarity() )
        {
            this.m_entries.splice( i, 0, entry );
            if ( this.m_entries.length > this.cacheSize() )
            {
                this.m_entries.pop();
            }
            return;
        }
    }
    if ( this.m_entries.length < this.cacheSize() )
    {
        this.m_entries.push( entry );
    }
}

/*
 * Returns the best entry that is currently in the cache and removes it
 * from the list.
 */
TrackCache.prototype.getTrack = function()
{
    if ( this.m_entries.length > 0 )
    {
        return this.m_entries.shift();
    }
    return null;
}

TrackCache.prototype.tick = function()
{
    for ( var i = this.m_entries.length - 1; i >= 0; i-- )
    {
        this.m_entries[ i ].tick();
        if ( this.m_entries[ i ].outdated() )
        {
            this.m_entries.splice( i, 1 );
        }
    }
}

TrackCache.prototype.showContents = function()
{
    this.m_table.show();
    this.updateGui();
}

/*
 * Update the GUI to the current state of the cache.
 */
TrackCache.prototype.updateGui = function()
{
    if ( !( this.m_table.visible ) )
    {
        return;
    }
    
    this.m_table.columnCount = 3;
    this.m_table.rowCount = this.m_entries.length;
    for ( var i = 0; i < this.m_entries.length; i++ )
    {
        var track = new QTableWidgetItem( this.m_entries[ i ].track().prettyName() );
        var value = new QTableWidgetItem( "" + this.m_entries[ i ].similarity() );
        var ttl = new QTableWidgetItem( "" + this.m_entries[ i ].ttl() );

        var rtl = parseInt( 100 / this.m_plugin.configuration.cacheMaxTTL() *
                            (this.m_plugin.configuration.cacheMaxTTL() - this.m_entries[ i ].ttl() ), 10 );
        var brush = new QBrush( Qt.SolidPattern );
        brush.setColor( new QColor( 255, 255 - rtl, 255 - rtl ) );
        
        track.setBackground( brush );
        value.setBackground( brush );
        ttl.setBackground(   brush );
        
        this.m_table.setItem( i, 0, value );
        this.m_table.setItem( i, 1, ttl );
        this.m_table.setItem( i, 2, track );
    }
    this.m_table.setWindowTitle( qsTranslate( "trackcache", "Track Cache" ) );
    this.m_table.setHorizontalHeaderLabels(
        new Array(
            qsTranslate( "trackcache", "Similarity" ),
            qsTranslate( "trackcache", "TTL" ),
            qsTranslate( "trackcache", "Track") ) );
    this.m_table.editTriggers = QAbstractItemView.NoEditTriggers;
    this.m_table.horizontalHeader().stretchLastSection = true;
}
