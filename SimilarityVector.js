/*
 *    Copyright (C) 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// PUBLIC


SimilarityVector.TrackAttribute                 = 0;
SimilarityVector.ArtistAttribute                = 1;
SimilarityVector.AlbumAttribute                 = 2;
SimilarityVector.ComposerAttribute              = 3;
SimilarityVector.GenreAttribute                 = 4;
SimilarityVector.YearAttribute                  = 5;
SimilarityVector.ScoreAttribute                 = 6;
SimilarityVector.RatingAttribute                = 7;
SimilarityVector.SimilarArtistsAttribute        = 8;
SimilarityVector.SimilarGenresAttribute         = 9;
SimilarityVector.MoodbarAttribute               = 10;

function SimilarityVector( plugin, url )
{
    this.m_plugin = plugin;
    this.m_url = url;
    
    this.m_components = new Array();

    // for automatically building the similar artists/genres table.
    this.m_artist = null;
    this.m_genre = null;
    this.m_similarArtists = new Array();
    this.m_similarGenres = new Array();
}

SimilarityVector.prototype.url = function()
{
    return this.m_url;
}

SimilarityVector.prototype.addComponent = function( component, similarity, weight )
{
    for ( var i = 0; i < this.m_components.length; i++ )
    {
        if ( this.m_components[ i ][ 0 ] == component )
        {
            this.m_components[ i ][ 1 ] += similarity;
            this.m_components[ i ][ 3 ] += 1.0;
            return;
        }
    }
    var c = new Array( component, similarity, weight, 1.0 );
    this.m_components.push( c );
}

/*
 * Set the weight of component to weight.
 * This is used for the learning algorithm to dynamically set the components weight
 * as computed in a single step of the algorithm.
 */
SimilarityVector.prototype.resetComponentWeight = function( component, weight )
{
    for ( var i = 0; i < this.m_components.length; i++ )
    {
        if ( this.m_components[ i ][ 0 ] == component )
        {
            this.m_components[ i ][ 2 ] = weight;
            return;
        }
    }
}

SimilarityVector.prototype.similarity = function()
{
    this.normalize();
    var sumSim = 0.0;
    var sumWeights = 0.0;
    for ( var i = 0; i < this.m_components.length; i++ )
    {
        sumSim += this.m_components[ i ][ 1 ] * this.m_components[ i ][ 2 ];
        sumWeights += this.m_components[ i ][ 2 ];
    }
    if ( sumWeights == 0.0 )
    {
        return 0.0;
    }
    return Math.min( 1.0, Math.max( 0.0, sumSim / sumWeights ) );
}

SimilarityVector.prototype.rate = function( goodChoice, learnAnyway )
{
    if ( this.m_plugin.configuration.learningEnabled() || learnAnyway )
    {
        this.normalize();
        this.sortBy( 1 );

        var gc = 1;
        if ( !( goodChoice ) )
        {
            gc = -1;
        }
        
        for ( var i = 0; i < this.m_components.length; i++ )
        {
            this.adjustAttributeWeight( this.m_components[ i ][ 0 ], ( i - this.m_components.length / 2 ) * gc );
        }
    }

    if ( goodChoice )
    {
        Amarok.Window.Statusbar.longMessage( qsTranslate( "similarityvector", "Got positive feedback. Weights adjusted." ) );
    } else
    {
        Amarok.Window.Statusbar.longMessage( qsTranslate( "similarityvector", "Got negative feedback. Weights adjusted." ) );
    }


    if ( this.m_artist != null )
    {
        for ( var i = 0; i < this.m_similarArtists.length; i++ )
        {
            if ( goodChoice )
            {
                this.m_plugin.playlist.addSimilarArtists( this.m_artist, this.m_similarArtists[ i ] );
            } else
            {
                this.m_plugin.playlist.decSimilarArtists( this.m_artist, this.m_similarArtists[ i ] );
            }
        }
    }
    if ( this.m_genre != null )
    {
        for ( var i = 0; i < this.m_similarGenres.length; i++ )
        {
            if ( goodChoice )
            {
                this.m_plugin.playlist.addSimilarGenres( this.m_genre, this.m_similarGenres[ i ] );
            } else
            {
                this.m_plugin.playlist.addSimilarGenres( this.m_genre, this.m_similarGenres[ i ] );
            }
        }
    }
}

SimilarityVector.prototype.add = function( other )
{
    for ( var i = 0; i < other.m_components.length; i++ )
    {
        this.addComponent( other.m_components[ i ][ 0 ], other.m_components[ i ][ 1 ], other.m_components[ i ][ 2 ] );
    }
}

SimilarityVector.prototype.setArtist = function( artist )
{
    this.m_artist = artist;
}

SimilarityVector.prototype.setGenre = function( genre )
{
    this.m_genre = genre;
}

SimilarityVector.prototype.addSimilarArtist = function( artist )
{
    if ( artist != null && artist != this.m_artist )
    {
        for ( var i = 0; i < this.m_similarArtists.length; i++ )
        {
            if ( this.m_similarArtists[ i ] == artist )
            {
                return;
            }
        }
        this.m_similarArtists.push( artist );
    }
}

SimilarityVector.prototype.addSimilarGenre = function( genre )
{
    if ( genre != null && genre != this.m_genre )
    {
        for ( var i = 0; i < this.m_similarGenres.length; i++ )
        {
            if ( this.m_similarGenres[ i ] == genre )
            {
                return;
            }
        }
        this.m_similarGenres.push( genre );
    }
}




// PRIVATE

SimilarityVector.prototype.adjustAttributeWeight = function( attribute, correction )
{
    if ( attribute == SimilarityVector.TrackAttribute )             this.m_plugin.configuration.adjustTrackWeight(          correction ); else
    if ( attribute == SimilarityVector.ArtistAttribute )            this.m_plugin.configuration.adjustArtistWeight(         correction ); else
    if ( attribute == SimilarityVector.AlbumAttribute )             this.m_plugin.configuration.adjustAlbumWeight(          correction ); else
    if ( attribute == SimilarityVector.ComposerAttribute )          this.m_plugin.configuration.adjustComposerWeight(       correction ); else
    if ( attribute == SimilarityVector.GenreAttribute )             this.m_plugin.configuration.adjustGenreWeight(          correction ); else
    if ( attribute == SimilarityVector.YearAttribute )              this.m_plugin.configuration.adjustYearWeight(           correction ); else
    if ( attribute == SimilarityVector.ScoreAttribute )             this.m_plugin.configuration.adjustScoreWeight(          correction ); else
    if ( attribute == SimilarityVector.RatingAttribute )            this.m_plugin.configuration.adjustRatingWeight(         correction ); else
    if ( attribute == SimilarityVector.MoodbarAttribute )           this.m_plugin.configuration.adjustMoodbarWeight(        correction ); else
    if ( attribute == SimilarityVector.SimilarArtistsAttribute )    this.m_plugin.configuration.adjustSimilarArtistWeight(  correction ); else
    if ( attribute == SimilarityVector.SimilarGenresAttribute )     this.m_plugin.configuration.adjustSimilarGenreWeight(   correction );

    this.m_plugin.configuration.saveConfig();
}

SimilarityVector.prototype.sortBy = function( idx )
{
    for ( var i = 0; i < this.m_components.length - 1; i++ )
    {
        for ( var j = i + 1; j < this.m_components.length; j++ )
        {
            if ( this.m_components[ i ][ idx ] > this.m_components[ j ][ idx ] )
            {
                var tmp = this.m_components[ i ];
                this.m_components[ i ] = this.m_components[ j ];
                this.m_components[ j ] = tmp;
            }
        }
    }
}

SimilarityVector.prototype.normalize = function()
{
    for ( var i = 0; i < this.m_components.length; i++ )
    {
        this.m_components[ i ][ 1 ] /= this.m_components[ i ][ 3 ];
        this.m_components[ i ][ 3 ] = 1.0;
    }
}