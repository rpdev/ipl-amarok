/*
 *    Copyright (C) 2009, 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

Importer.include( "Track.js" );
Importer.include( "SimilarityVector.js" );

/*
 * How many hits must a artist or genre pair collect to be visible in the suggestions dialog?
 */
Playlist.MinimalHitsBeforeShown = 10;

/*
 * Constructs a new Collection object.
 * The Collection class provides some functionality to query the Collection,
 * returning our own Track data type.
 */
function Playlist( plugin )
{
    Amarok.debug( "Creating new Playlist object." );
    this.m_plugin = plugin;
    this.m_vectors = new Array();

    // for gestures
    this.m_volume = Amarok.Engine.volume;

    Amarok.Engine[ 'volumeChanged(int)' ].connect( this, this.volumeChanged );

    // for automatically building the similar artists/genres tables
    this.m_similarArtists = new Array();
    this.m_similarGenres = new Array();

    this.restoreState();
}

/*
 * Returns the current playing track.
 */
Playlist.prototype.currentTrack = function()
{
    return this.amarokTrackToTrack( Amarok.Engine.currentTrack() );
}

/*
 * Returns the tracks currently in the playlist.
*/
Playlist.prototype.playlist = function()
{
    var result = new Array();
    for ( var i = 0; i < Amarok.Playlist.totalTrackCount(); i++ )
    {
        result.push( this.amarokTrackToTrack( Amarok.Playlist.trackAt( i ) ) );
    }
    return result;
}

/*
 * Translates a "Amarok" track object into our own one.
 */
Playlist.prototype.amarokTrackToTrack = function( track )
{
    var result = new Track( -1 );
    result.fillData(
        "file://" + track.path,
        track.title,
        track.artist,
        track.album,
        track.composer,
        track.genre,
        track.year,
        track.score,
        track.rating
    );
    return result;
}

/*
 * Returns the number of upcoming tracks in the playlist.
 */
Playlist.prototype.upcomingTracks = function()
{
    var total = Amarok.Playlist.totalTrackCount();
    return total - this.playedTracks();
}

/*
 * Returns the number of tracks already played in the playlist.
 */
Playlist.prototype.playedTracks = function()
{
    return Amarok.Playlist.activeIndex() + 1;
}

/*
 * Removes as many tracks from the beginning of the playlist, until
 * the playedTracks() is less or equal than the value the
 * user entered in the configuration.
 */
Playlist.prototype.removePlayedTracks = function()
{
    while ( this.playedTracks() > this.m_plugin.configuration.playedTracks() )
    {
        Amarok.Playlist.removeByIndex( 0 );
    }
}

/*
 * Add a new similarity vector to the cache.
 */
Playlist.prototype.addVector = function( vector )
{
    this.updateVectorCache();
    this.m_vectors.push( vector );
}

/*
 * Approve the currently played track.
 */
Playlist.prototype.approveCurrentTrack = function()
{
    var v = this.findVectorForUrl( this.currentTrack().toFileName(), true );
    if ( v != null )
    {
        v.rate( true, true );
    }
}

/*
 * Disapprove the currently playling track.
 */
Playlist.prototype.disapproveCurrentTrack = function()
{
    var v = this.findVectorForUrl( this.currentTrack().toFileName(), true );
    if ( v != null )
    {
        v.rate( false, true );
    }
}

/*
 * Check, whether the playlist contains track.
 * This will rather check the playlist passed in pl.
 * However, if pl is null, the current playlist will be used.
 * This is to allow reducing the number of calls to
 * playlist().
 */
Playlist.prototype.containsTrack = function( track, pl )
{
    if ( pl == null )
    {
        pl = this.playlist();
    }
    for ( var i = 0; i < pl.length; i++ )
    {
        if ( track.toFileName() == pl[ i ].toFileName() )
        {
            return true;
        }
    }
    return false;
}

/*
 * Adds an entry to the similar artists table or increased an entries count.
 * If the entry is not yet in the relation, it is added and gets an initial value
 * of one assigned. If the entry is already in the relation, its value is increased by
 * one.
 *
 * This table is used to suggest the user artists for the (global) similar artists relation.
 */
Playlist.prototype.addSimilarArtists = function( artist1, artist2 )
{
    if ( this.m_plugin.configuration.areSimilarArtists( artist1, artist2 ) )
    {
        return;
    }
    
    for ( var i = 0; i < this.m_similarArtists.length; i++ )
    {
        if ( ( this.m_similarArtists[ i ][ 0 ][ 0 ] == artist1 && this.m_similarArtists[ i ][ 0 ][ 1 ] == artist2 ) ||
             ( this.m_similarArtists[ i ][ 0 ][ 0 ] == artist2 && this.m_similarArtists[ i ][ 0 ][ 1 ] == artist1 ) )
        {
             this.m_similarArtists[ i ][ 1 ] += 1;
             this.saveState();
             return;
        }
    }
    var entry = new Array();
    entry.push( new Array( artist1, artist2 ) );
    entry.push( 1 );
    this.m_similarArtists.push( entry );
    this.saveState();
}

/*
 * Decreases the number of hits of a pair of similar artists.
 * If the count reaches zero, the pair is removed from the list.
 */
Playlist.prototype.decSimilarArtists = function( artist1, artist2 )
{
    for ( var i = 0; i < this.m_similarArtists.length; i++ )
    {
        if ( ( this.m_similarArtists[ i ][ 0 ][ 0 ] == artist1 && this.m_similarArtists[ i ][ 0 ][ 1 ] == artist2 ) ||
             ( this.m_similarArtists[ i ][ 0 ][ 0 ] == artist2 && this.m_similarArtists[ i ][ 0 ][ 1 ] == artist1 ) )
        {
             this.m_similarArtists[ i ][ 1 ] -= 1;
             if ( this.m_similarArtists[ i ][ 1 ] <= 0 )
             {
                this.m_similarArtists.splice( i, 1 );
             }
             this.saveState();
             return;
        }
    }
}

/*
 * Adds an entry to the similar genres table or increased an entries count.
 * If the entry is not yet in the relation, it is added and gets an initial value
 * of one assigned. If the entry is already in the relation, its value is increased by
 * one.
 *
 * This table is used to suggest the user genres for the (global) similar genres relation.
 */
Playlist.prototype.addSimilarGenres = function( genre1, genre2 )
{
    if ( this.m_plugin.configuration.areSimilarGenres( genre1, genre2 ) )
    {
        return;
    }
    
    for ( var i = 0; i < this.m_similarGenres.length; i++ )
    {
        if ( ( this.m_similarGenres[ i ][ 0 ][ 0 ] == genre1 && this.m_similarGenres[ i ][ 0 ][ 1 ] == genre2 ) ||
             ( this.m_similarGenres[ i ][ 0 ][ 0 ] == genre2 && this.m_similarGenres[ i ][ 0 ][ 1 ] == genre1 ) )
        {
            this.m_similarGenres[ i ][ 1 ] += 1;
            this.saveState();
            return;
        }
    }
    var entry = new Array();
    entry.push( new Array( genre1, genre2 ) );
    entry.push( 1 );
    this.m_similarGenres.push( entry );
    this.saveState();
}

/*
 * Decreases the count of a pair of genres in the similar genres relation.
 * If the counter reaches zero, the pair is removed.
 */
Playlist.prototype.decSimilarGenres = function( genre1, genre2 )
{
    for ( var i = 0; i < this.m_similarGenres.length; i++ )
    {
        if ( ( this.m_similarGenres[ i ][ 0 ][ 0 ] == genre1 && this.m_similarGenres[ i ][ 0 ][ 1 ] == genre2 ) ||
             ( this.m_similarGenres[ i ][ 0 ][ 0 ] == genre2 && this.m_similarGenres[ i ][ 0 ][ 1 ] == genre1 ) )
        {
            this.m_similarGenres[ i ][ 1 ] += 1;
            if ( this.m_similarGenres[ i ][ 1 ] <= 0 )
            {
                this.splice( i, 1 );
            }
            this.saveState();
            return;
        }
    }
}

/*
 * Removes the given pair of artists from the collected statistics data list.
 * If addToGlobal is true, the artists will be inserted in the global similar
 * artists relation.
 */
Playlist.prototype.removeSimilarArtists = function( artist1, artist2, addToGlobal )
{
    for ( var i = 0; i < this.m_similarArtists.length; i++ )
    {
        if ( ( this.m_similarArtists[ i ][ 0 ][ 0 ] == artist1 && this.m_similarArtists[ i ][ 0 ][ 1 ] == artist2 ) ||
             ( this.m_similarArtists[ i ][ 0 ][ 0 ] == artist2 && this.m_similarArtists[ i ][ 0 ][ 1 ] == artist1 ) )
        {
            if ( addToGlobal )
            {
                this.m_plugin.configuration.setSimilarArtistsAndSave( artist1, new Array( artist2 ) );
            }
            this.m_similarArtists.splice( i, 1 );
            this.saveState();
            return;
        }
    }
}

/*
 * Removes the given pair of genres from the collected statistics data list.
 * If addToGlobal is true, the genres will be inserted in the global similar
 * genres relation.
 */
Playlist.prototype.removeSimilarGenres = function( genre1, genre2, addToGlobal )
{
    for ( var i = 0; i < this.m_similarGenres.length; i++ )
    {
        if ( ( this.m_similarGenres[ i ][ 0 ][ 0 ] == genre1 && this.m_similarGenres[ i ][ 0 ][ 1 ] == genre2 ) ||
             ( this.m_similarGenres[ i ][ 0 ][ 0 ] == genre2 && this.m_similarGenres[ i ][ 0 ][ 1 ] == genre1 ) )
        {
            if ( addToGlobal )
            {
                this.m_plugin.configuration.setSimilarGenresAndSave( genre1, new Array( genre2 ) );
            }
            this.m_similarGenres.splice( i, 1 );
            this.saveState();
            return;
        }
    }
}

/*
 * Returns the next pair of similar artists from the statistics list.
 * If there are no more artists, null is returned.
 */
Playlist.prototype.nextSimilarArtists = function()
{
    var idx = -1;
    var score = 0;

    for ( var i = 0; i < this.m_similarArtists.length; i++ )
    {
        if ( this.m_similarArtists[ i ][ 1 ] > score )
        {
            score = this.m_similarArtists[ i ][ 1 ];
            idx = i;
        }
    }
    if ( idx >= 0 && score >= Playlist.MinimalHitsBeforeShown )
    {
        return this.m_similarArtists[ idx ][ 0 ];
    } else
    {
        return null;
    }
}

/*
 * Returns the next pair of similar genres from the statistics list.
 * If there are no more genres, null is returned.
 */
Playlist.prototype.nextSimilarGenres = function()
{
    var idx = -1;
    var score = 0;

    for ( var i = 0; i < this.m_similarGenres.length; i++ )
    {
        if ( this.m_similarGenres[ i ][ 1 ] > score )
        {
            score = this.m_similarGenres[ i ][ 1 ];
            idx = i;
        }
    }
    if ( idx >= 0 && score >= Playlist.MinimalHitsBeforeShown )
    {
        return this.m_similarGenres[ idx ][ 0 ];
    } else
    {
        return null;
    }
}






// PRIVATE



/*
 * Find similarity vector for the given url.
 * If remove is true, the vector will be removed from the list
 * of stored vectors.
 */
Playlist.prototype.findVectorForUrl = function( url, remove )
{
    for ( var i = 0; i < this.m_vectors.length; i++ )
    {
        if ( ( "" + this.m_vectors[ i ].url() ) == ( "" + url ) )
        {
            if ( remove )
            {
                return this.m_vectors.splice( i, 1 )[ 0 ];
            } else
            {
                return this.m_vectors[ i ];
            }
        }
    }
    return null;
}

/*
 * Updates the vector cache.
 * This will remove any vector, that whose corresponding url is any more
 * to be found in the playlist.
 */
Playlist.prototype.updateVectorCache = function()
{
    var pl = this.playlist();
    for ( var i = 0; i < this.m_vectors.length; i++ )
    {
        var found = false;
        for ( var j = 0; j < pl.length; j++ )
        {
            if ( ( this.m_vectors[ i ].url() ) == ( pl[ j ].toFileName() ) )
            {
                found = true;
                break;
            }
        }
        if ( !( found ) )
        {
            this.m_vectors.splice( i, 1 );
        }
    }
}

/*
 * This is fired whenever the volume changed.
 * Used as a gesture, i.e. turning volume up is
 * considered positive feedback.
 */
Playlist.prototype.volumeChanged = function( volume )
{
    var delta = volume - this.m_volume;
    if ( delta > 0 )
    {
        var v = this.findVectorForUrl( this.currentTrack().toFileName(), true );
        if ( v != null )
        {
            v.rate( true, false );
        }
    }
    this.m_volume = volume;
}

/*
 * Save the collected statistics to disk.
 */
Playlist.prototype.saveState = function()
{
    var value = "";
    for ( var i = 0; i < this.m_similarArtists.length; i++ )
    {
        var line = "" + this.m_similarArtists[ i ][ 0 ][ 0 ] + "|||" +
                        this.m_similarArtists[ i ][ 0 ][ 1 ] + "|||" +
                        this.m_similarArtists[ i ][ 1 ];
        if ( value.length > 0 )
        {
            value += ";;;";
        }
        value += line;
    }
    Amarok.Script.writeConfig( "playlist_similarArtists", value );

    value = "";
    for ( var i = 0; i < this.m_similarGenres.length; i++ )
    {
        var line = "" + this.m_similarGenres[ i ][ 0 ][ 0 ] + "|||" +
                        this.m_similarGenres[ i ][ 0 ][ 1 ] + "|||" +
                        this.m_similarGenres[ i ][ 1 ];
        if ( value.length > 0 )
        {
            value += ";;;";
        }
        value += line;
    }
    Amarok.Script.writeConfig( "playlist_similarGenres", value );
}

/*
 * Restores the collected statistics data.
 */
Playlist.prototype.restoreState = function()
{
    this.m_similarArtists = new Array();
    var entries = Amarok.Script.readConfig( "playlist_similarArtists", "" ).split( ";;;" );
    for ( var i = 0; i < entries.length; i++ )
    {
        var entry = new Array();
        var elist = entries[ i ].split( "|||" );
        entry.push( new Array( elist[ 0 ], elist[ 1 ] ) );
        entry.push( parseInt( elist[ 2 ], 10 ) );
        this.m_similarArtists.push( entry );
    }

    this.m_similarGenres = new Array();
    entries = Amarok.Script.readConfig( "playlist_similarGenres", "" ).split( ";;;" );
    for ( var i = 0; i < entries.length; i++ )
    {
        var entry = new Array();
        var elist = entries[ i ].split( "|||" );
        entry.push( new Array( elist[ 0 ], elist[ 1 ] ) );
        entry.push( parseInt( elist[ 2 ], 10 ) );
        this.m_similarGenres.push( entry );
    }
}