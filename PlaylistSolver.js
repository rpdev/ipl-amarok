/*
 *    Copyright (C) 2009, 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

Importer.include( "ExplicitSimilarityList.js" );
Importer.include( "Track.js" );
Importer.include( "TrackCacheEntry.js" );
Importer.include( "StringComparer.js" );
Importer.include( "SimilarityVector.js" );

/*
 * Creates a new playlist solver.
 * The solves calculates playlists based on given input.
 */
function PlaylistSolver( plugin )
{
    Amarok.debug( "Creating new PlaylistSolver object." );
    this.m_plugin = plugin;

    this.m_stringComparer = new StringComparer( plugin );

    Amarok.Engine[ 'trackChanged()' ].connect( this, this.doTrackChanged );

    this.m_timer = new QTimer( this );
    this.m_timer.interval = 2000;
    this.m_timer['timeout()'].connect( this, this.doUpdatePlaylist );
}

/*
 * Compare two tracks and return a similarity value.
 * track1 is assumed to be a track for beeing testes it matches track2,
 * i.e. track2 is the known track (e.g. currently in playlist).
 * However, the algorithm used is symmetric, thus, this rule is just for
 * backtracking issues.
 *
 * The method will normally follow the selected attribute policy set by the user.
 * If forceCmpAttr is true, all known attributes will be evaluated (no matter what the user has set).
 */
PlaylistSolver.prototype.compareTracks = function( track1, track2, forceCmpAttr )
{
    var vector = new SimilarityVector( this.m_plugin, track1.toFileName() );
    
    // Check track title.
    if ( this.m_plugin.configuration.includeTrack() || forceCmpAttr )
    {
        vector.addComponent( SimilarityVector.TrackAttribute,
                             this.compareString( track1.track(), track2.track(), this.m_plugin.configuration.matchTrackStrict() ),
                             this.m_plugin.configuration.trackWeight() );
    }

    // Check artist.
    if ( this.m_plugin.configuration.includeArtist() || forceCmpAttr )
    {
        vector.addComponent( SimilarityVector.ArtistAttribute,
                             this.compareString( track1.artist(), track2.artist(), this.m_plugin.configuration.matchArtistStrict() ),
                             this.m_plugin.configuration.artistWeight() );
        vector.addComponent( SimilarityVector.SimilarArtistsAttribute,
                             this.compareExplicitSimilarity( track1.artist(), track2.artist(), Configuration.SimilarArtists ),
                             this.m_plugin.configuration.similarArtistsWeight() ); 
    }

    // Check album.
    if ( this.m_plugin.configuration.includeAlbum() || forceCmpAttr )
    {
        vector.addComponent( SimilarityVector.AlbumAtribute,
                             this.compareString( track1.album(), track2.album(), this.m_plugin.configuration.matchAlbumStrict() ),
                             this.m_plugin.configuration.albumWeight() );
    }

    // Check composer.
    if ( this.m_plugin.configuration.includeComposer() || forceCmpAttr )
    {
        vector.addComponent( SimilarityVector.ComposerAttribute,
                             this.compareString( track1.composer(), track2.composer(), this.m_plugin.configuration.matchComposerStrict() ),
                             this.m_plugin.configuration.composerWeight() );
    }

    // Check genre.
    if ( this.m_plugin.configuration.includeGenre() || forceCmpAttr )
    {
        vector.addComponent( SimilarityVector.GenreAttribute,
                             this.compareString( track1.genre(), track2.genre(), this.m_plugin.configuration.matchGenreStrict() ),
                             this.m_plugin.configuration.genreWeight() );
        vector.addComponent( SimilarityVector.SimilarGenresAttribute,
                             this.compareExplicitSimilarity( track1.genre(), track2.genre(), Configuration.SimilarGenres ),
                             this.m_plugin.configuration.similarGenresWeight() );
    }

    // Check year.
    if ( this.m_plugin.configuration.includeYear() || forceCmpAttr )
    {
        vector.addComponent( SimilarityVector.YearAttribute,
                             this.compareNumber( track1.year(), track2.year(), this.m_plugin.configuration.yearMaxDistance() ),
                             this.m_plugin.configuration.yearWeight() );
    }

    // Check score.
    if ( this.m_plugin.configuration.includeScore() || forceCmpAttr )
    {
        vector.addComponent( SimilarityVector.ScoreAttribute, 
                             this.compareNumber( track1.score(), track2.score(), this.m_plugin.configuration.scoreMaxDistance() ),
                             this.m_plugin.configuration.scoreWeight() );
    }

    // Check rating.
    if ( this.m_plugin.configuration.includeRating() || forceCmpAttr )
    {
        vector.addComponent( SimilarityVector.RatingAttribute,
                             this.compareNumber( track1.rating(), track2.rating(), this.m_plugin.configuration.ratingMaxDistance() ),
                             this.m_plugin.configuration.ratingWeight() );
    }

    // Check moodbars
    if ( this.m_plugin.configuration.includeMoodbar() || forceCmpAttr )
    {
        vector.addComponent( SimilarityVector.MoodbarAttribute,
                             this.m_plugin.moodbarManager.compareTracks( track1, track2, false ),
                             this.m_plugin.configuration.moodbarWeight() );
    }

    return vector;
}

/*
 * Compare the two strings string1 and string2 and returns a similarity value
 * which is between 0.0 and 1.0.
 * The method supports two comparison modes:
 * strict - Return either 0.0 or 1.0, depending on whether the two strings are
 *          completely equal (1.0) or differen (0.0).
 * fuzzy  - Returns a fuzzy value depending on the selected algorithm in the configuration
 */
PlaylistSolver.prototype.compareString = function( string1, string2, useStrictMatching )
{
    if ( useStrictMatching )
    {
        var result = 0.0;
        if ( string1 == string2 )
        {
            result = 1.0;
        }
        Amarok.debug( "Comparing [" + string1 + "] and [" + string2 + "] using strict mode: " + result );
        return result;
    } else
    {
        //TODO: Make this configurable via Configuration?
        var result = 0.0;
        if ( this.m_plugin.configuration.fuzzyStringMatchingAlgorithm() == Configuration.LevenshteinDistance )
        {
            result = this.levenshteinDistance( string1, string2 );
        } else
        {
            result = this.m_stringComparer.compareStrings( string1, string2 );
        }
        Amarok.debug( "Comparing [" + string1 + "] and [" + string2 + "] using fuzzy mode: " + result );
        return result;
    }
}

/*
 * Compare two numbers number1 and number2.
 * The method returns a value between 0.0 and 1.0, where 1.0 means, the two
 * numbers are exactly equal.
 * If they are not, equal, the result is the normalized linear distance between
 * both values. If the distance is greater or equal than
 * max distance, 0.0 is returned, otherwise the result will be between 1.0 and
 * 0.0.
 */
PlaylistSolver.prototype.compareNumber = function( number1, number2, maxDistance )
{
    var dist = Math.abs( number1 - number2 );
    dist = Math.min( dist, maxDistance );
    if ( maxDistance == 0 )
    {
        return 0.0;
    }
    var result = ( maxDistance - dist ) / maxDistance;
    Amarok.debug( "Comparing numbers [" + number1 + "] and [" + number2 + "] : " + result );
    return result;
}

/**
 * Calculate similarity of explicitly set values.
 * This internally uses the ExplicitSimilarityList solver class to solve the problem,
 * which is basically a breadth first search, starting with value1 as the root and searching
 * for value2.
 * On each level, the similarity to value1 is decreased, to prevent
 * infinite depth in the search graph.
 *
 * Mode specifies the mode to use (e.g. compare artists, genres)
 */
PlaylistSolver.prototype.compareExplicitSimilarity = function( value1, value2, mode )
{
    var solver = new ExplicitSimilarityList( this.m_plugin, value1, value2, mode );
    var result = solver.solve();
    Amarok.debug( "Comparing [" + value1 + "] and [" + value2 + "] using mode[" + mode + "]: " + result );
    return result;
}

/*
 * Calculate the normalized edit distance as described by Levenshtein.
 * This compares the two strings string1 and string2, using the Levenshtein
 * distance (which basically is an extended version of the Hamming distance).
 * The return value will be between 1.0 (the strings are equal) and 0.0
 * (the strings are completely different.
 */
PlaylistSolver.prototype.levenshteinDistance = function( string1, string2 )
{
    if ( string1 == null || string2 == null || string1.length == 0 || string2.length == 0 )
    {
        return 0.0;
    }
    var matrix = new Array( string1.length );
    for ( var i = 0; i < string1.length; i++ )
    {
        matrix[ i ] = new Array( string2.length );
        matrix[ i ][ 0 ] = 0;
    }
    for ( var i = 0; i < string2.length; i++ )
    {
        matrix[ 0 ][ i ] = i;
    }

    for ( var col = 1; col < string2.length; col++ )
    {
        for ( var row = 1; row < string1.length; row++ )
        {
            if ( string1.charAt( row ) == string2.charAt( col ) )
            {
                matrix[ row ][ col ] = matrix[ row - 1][ col - 1 ];
            } else
            {
                var v1 = matrix[ row - 1 ][ col     ] + 1;
                var v2 = matrix[ row     ][ col - 1 ] + 1;
                var v3 = matrix[ row - 1 ][ col - 1 ] + 1;
                matrix[ row ][ col ] = Math.min( v1, v2, v3 );
            }
        }
    }
    var distance = matrix[ string1.length - 1 ][ string2.length - 1 ];
    var maxDist = Math.max( string1.length, string2.length );
    var result = 1.0 - parseFloat( distance ) / parseFloat( maxDist );
    return Math.max( 0.0, Math.min( 1.0, result ) );
}

/*
 * Check if two tracks are in a group.
 *
 * A "group" is defined by Amaroks custom "Label" system, where a user can
 * assign labels to each track. The Agent assumes the user to
 * use this feature rarely, i.e. tag all "instrumental" tracks using it.
 *
 * This returns true if the tracks share at least one group, i.e.
 * if at least one label exists, so that it is part of the labels list of
 * track1 and of the labels list of track2.
 */
PlaylistSolver.prototype.areInSameGroup = function( track1, track2 )
{
    if ( track1 != null && track2 != null )
    {
        var l1 = track1.labels();
        var l2 = track2.labels();
        for ( var i = 0; i < l1.length; i++ )
        {
            for ( var j = 0; j < l2.length; j++ )
            {
                if ( l1[ i ] == l2[ j ] )
                {
                    return true;
                }
            }
        }
    }
    return false;
}

/*
 * Search for the next track.
 * This will iterate over the collection to find a track, that is similar to
 * at last one track in the playlist.
 * The method will stop either when it found a track that satisfies the
 * settings given by the user or if a maximum search depth has been
 * reached. In the ladder case, the suggested track will be on best effort,
 * i.e. the track with the highest calculated similarity value will be
 * suggested.
 * The method appends the suggested track to the playlist.
 */
PlaylistSolver.prototype.findNextTrack = function()
{
    this.m_plugin.cache.tick(); // remove outdated tracks
    var playlist = this.m_plugin.playlist.playlist();
    for ( var loops = 0; loops < this.m_plugin.configuration.maximumLoops(); loops++ )
    {
        var randomTrack = this.m_plugin.collection.randomTrack();
        if ( this.m_plugin.playlist.containsTrack( randomTrack, playlist ) )
        {
            continue;
        }
        
        /*
         * If we have found a track that is in the same group as one in the
         * playlist, immediately append it to the playlist.
         * Just for convenience, compare it to the track from the same group.
         * Currently, this might be rather useless, however, maybe I someday add
         * some further weighting for this too and integrate it into the similarity
         * calculation. Until then, keep the grouping separated from the rest.
         */
        if ( this.m_plugin.configuration.honorGroups() )
        {
            for ( var i = 0; i < playlist.length; i++ )
            {
                if ( this.areInSameGroup( playlist[ i ], randomTrack ) )
                {
                    randomTrack.appendToPlaylist();
                    this.m_plugin.playlist.addVector( this.compareTracks( randomTrack, playlist[ i ], false ) );
                    return;
                }
            }
        }

        /*
         * If we find a track, which has some attributes that the user explicitly marked as similar,
         * immediately append it to the playlist.
         * This bypasses the remaining similarity calculation. This is useful if the user uses a lot
         * of explicit similarities. However, this might tend to create really specialized playlists, so
         * better don't use and instead let the similarity checking bring in a bit "random" tracks.
         */
        if ( this.m_plugin.configuration.honorExplicits() )
        {
            for ( var i = 0; i < playlist.length; i++ )
            {
                if ( this.m_plugin.configuration.areSimilarArtists( randomTrack.artist(), playlist[ i ].artist() ) ||
                     this.m_plugin.configuration.areSimilarGenres( randomTrack.genre(), playlist[ i ].genre() ) )
                {
                    randomTrack.appendToPlaylist();
                    this.m_plugin.playlist.addVector( this.compareTracks( randomTrack, playlist[ i ], false ) );
                    return;
                }
            }
        }
        
        if ( this.m_plugin.configuration.matchCompletePlaylist() )
        {
            var sim = null;
            for ( var i = 0; i < playlist.length; i++ )
            {
                var s = this.compareTracks( randomTrack, playlist[ i ], false );
                if ( sim == null )
                {
                    sim = s;
                    sim.setArtist( randomTrack.artist() );
                    sim.setGenre( randomTrack.genre() );
                } else
                {
                    sim.add( s );
                }
                sim.addSimilarArtist( playlist[ i ].artist() );
                sim.addSimilarGenre( playlist[ i ].genre() );
            }
            if ( sim == null )
            {
                continue;
            }
            var ssim = sim.similarity();
            if ( ssim > this.m_plugin.configuration.minimalSimilarity() )
            {
                randomTrack.appendToPlaylist();
                this.m_plugin.playlist.addVector( sim );
                return;
            }
            this.m_plugin.cache.insertTrack( new TrackCacheEntry( randomTrack,
                                                                  ssim,
                                                                  this.m_plugin.configuration.cacheMaxTTL(),
                                                                  sim) );
        } else
        {
            var bestVec = null;
            var bestSim = -1.0;
            for ( var i = 0; i < playlist.length; i++ )
            {
                var vector = this.compareTracks( randomTrack, playlist[ i ], false );
                var sim = vector.similarity();
                vector.setArtist( randomTrack.artist() );
                vector.setGenre( randomTrack.genre() );
                vector.addSimilarArtist( playlist[ i ].artist() );
                vector.addSimilarGenre( playlist[ i ].genre() );
                if ( sim >= this.m_plugin.configuration.minimalSimilarity() )
                {
                    randomTrack.appendToPlaylist();
                    this.m_plugin.playlist.addVector( vector );
                    return;
                }
                if ( sim > bestSim )
                {
                    bestVec = vector;
                    bestSim = sim;
                }
            }
            this.m_plugin.cache.insertTrack( new TrackCacheEntry( randomTrack,
                                                                  bestSim,
                                                                  this.m_plugin.configuration.cacheMaxTTL(),
                                                                  bestVec) );
        }
    }
    var bestMatch = this.m_plugin.cache.getTrack();
    if ( bestMatch != null )
    {
        bestMatch.track().appendToPlaylist();
        this.m_plugin.playlist.addVector( bestMatch.vector() );
    }
}

/*
 * The current playling track has changed.
 * This method is automatically called when the current playling track has
 * changed.
 */
PlaylistSolver.prototype.doTrackChanged = function()
{
    this.m_timer.start();
}

/*
 * Update the playlist, i.e. remove old tracks and find new (similar) ones.
 */
PlaylistSolver.prototype.doUpdatePlaylist = function()
{
    this.m_timer.stop();
    
    try
    {
        if ( Amarok.Engine.engineState() == 0 )
        {
            if ( this.m_plugin.configuration.intelligentPlaylistEnabled() )
            {
                this.m_plugin.playlist.removePlayedTracks();
                var runs = this.m_plugin.configuration.upcomingTracks() - this.m_plugin.playlist.upcomingTracks();
                while ( runs > 0 &&
                        this.m_plugin.playlist.upcomingTracks() < this.m_plugin.configuration.upcomingTracks() )
                {
                    runs--;
                    this.findNextTrack();
                }
            }
        }
    } catch ( ex )
    {
        printStackTrace( ex );
    }
}