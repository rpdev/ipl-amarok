/*
 *    Copyright (C) 2009, 2010 by Martin Höher <martin@rpdev.net>    
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
Importer.loadQtBinding( "qt.core" );
Importer.loadQtBinding( "qt.gui" );

function printStackTrace( ex )
{
    var err = "There was an error in the Intelligent Playlist plugin!\n\n";

    err += "Error: " + ex.toString() + "\n"
    if (ex.name)
    {
        err += "Error Name: " + ex.name + "\n";
    }
    if (ex.message)
    {
        err += "Message: " + ex.message + "\n";
    }
    if (ex.fileName)
    {
        err +="File: " + ex.fileName + "\n";
    }
    if (ex.lineNumber)
    {
        err += "Line: " + ex.lineNumber + "\n";
    }
    if (ex.stack)
    {
        err += "stack:\n" + ex.stack + "\n";
    }

    Amarok.alert(err);
}

try
{
    Importer.include( "Translation.js" );
    installTranslator();
    Importer.include( "IntelligentPlaylist.js" );
    intelligentPlaylist = new IntelligentPlaylist();
}
catch ( ex )
{
    printStackTrace( ex );
}