/*
 *    Copyright (C) 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Creates a new TrackCacheEntry.
 * Each object represents a entry in a TrackCache object.
 * Basically, a track cache entry holds the track, the calculated
 * similarity and maybe other data.
 *
 * The Time To Live (TTL) value passed indicated the maximum life time of an
 * entry. This prevents the cache to be filled with outdated tracks (i.e. tracks
 * that were selected due to tracks in the playlist a long time ago.
 */
function TrackCacheEntry( track, similarity, ttl, vector )
{
    this.m_track = track;
    this.m_similarity = similarity;
    this.m_ttl = ttl;
    this.m_vector = vector;
}

TrackCacheEntry.prototype.track = function()
{
    return this.m_track;
}

TrackCacheEntry.prototype.similarity = function()
{
    return this.m_similarity;
}

TrackCacheEntry.prototype.ttl = function()
{
    return this.m_ttl;
}

TrackCacheEntry.prototype.vector = function()
{
    return this.m_vector;
}

TrackCacheEntry.prototype.tick = function()
{
    this.m_ttl = this.m_ttl - 1;
}

TrackCacheEntry.prototype.outdated = function()
{
    return ( this.m_ttl <= 0 );
}