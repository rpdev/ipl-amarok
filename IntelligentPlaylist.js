/*
 *    Copyright (C) 2009, 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
Importer.include( "Collection.js" );
Importer.include( "Configuration.js" );
Importer.include( "MoodbarManager.js" );
Importer.include( "Playlist.js" );
Importer.include( "PlaylistSolver.js" );
Importer.include( "SimilarTracksAnalyzer.js" );
Importer.include( "SimilarWidget.js" );
Importer.include( "TrackCache.js" );

function IntelligentPlaylist() 
{
    Amarok.debug( "IntelligentPlaylist.IntelligentPlaylist" );

    this.collection = new Collection( this );
    this.playlist = new Playlist( this );
    this.moodbarManager = new MoodbarManager( this );
    this.solver = new PlaylistSolver( this );
    this.cache = new TrackCache( this );
    this.applet = new SimilarWidget( this );
    this.similarTracksAnalyzer = new SimilarTracksAnalyzer( this );

    // Keep this the last in the chain. It most likely needs one of the
    // other components.
    this.configuration = new Configuration( this );
}

