/*
 *    Copyright (C) 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


Importer.loadQtBinding( "qt.core" );
Importer.loadQtBinding( "qt.gui" );
Importer.loadQtBinding( "qt.uitools" );


// PUBLIC

function SimilarForm( plugin )
{
    this.m_plugin = plugin;

    this.m_value = null;
    this.m_values = new Array();
    this.m_list = new Array();
    this.m_mode = "";

    // Load UI
    var uiLoader = new QUiLoader( this );
    var uifile = new QFile( Amarok.Info.scriptPath() + "/similarform.ui" );
    uifile.open( QIODevice.ReadOnly );
    this.m_configForm = uiLoader.load( uifile, this );
    uifile.close();

    //this.m_configForm.filter.setListWidget( this.m_configForm.values );
    this.m_configForm.filter['textChanged(const QString&)'].connect( this, this.applyFilter );
    this.m_configForm.buttonBox['accepted()'].connect( this, this.doApply );
    this.m_configForm.buttonBox['rejected()'].connect( this, this.doCancel );
}

SimilarForm.prototype.show = function( mode, value, values, list )
{
    this.m_mode = mode;
    this.m_value = value;
    this.m_values = values;
    this.m_list = list;
    this.refreshGui();
    this.m_configForm.filter.text = "";
    this.m_configForm.show();
}



// PRIVATE

SimilarForm.prototype.update = function()
{
    this.m_values = new Array();
    for ( var i = 0; i < this.m_configForm.values.count; i++ )
    {
        var it = this.m_configForm.values.item( i );
        if ( it.checkState() == Qt.Checked )
        {
            this.m_values.push( it.text() );
        }
    }
}

SimilarForm.prototype.refreshGui = function()
{
    // Set up "Similar Artists" tab
    this.m_configForm.label.text = this.modeToCaption();
    this.m_configForm.value.text = this.m_value;
    this.m_configForm.setWindowTitle( this.modeToCaption() + " " + this.m_value );
    while ( this.m_configForm.values.count > 0 )
    {
        this.m_configForm.values.takeItem( 0 );
    }
    for ( var i = 0; i < this.m_list.length; i++ )
    {
        it = new QListWidgetItem();
        it.setText( this.m_list[ i ] );
        if ( this.isSimilar( this.m_list[ i ] ) )
        {
            it.setCheckState( Qt.Checked );
        } else
        {
            it.setCheckState( Qt.Unchecked );
        }
        this.m_configForm.values.addItem( it );
    }
}

SimilarForm.prototype.isSimilar = function( v )
{
    for ( var i = 0; i < this.m_values.length; i++ )
    {
        if ( v == this.m_values[ i ] )
        {
            return true;
        }
    }
    return false;
}

SimilarForm.prototype.modeToCaption = function()
{
    if ( this.m_mode == Configuration.SimilarArtists )
    {
        return qsTranslate( "similarform", "Similar Artists for" );
    }
    if ( this.m_mode == Configuration.SimilarGenres )
    {
        return qsTranslate( "similarform", "Similar Genres for" );
    }
    return "";
}


// PRIVATE SLOTS

SimilarForm.prototype.doApply = function()
{
    this.update();
    this.m_configForm.close();
    if ( this.m_mode == Configuration.SimilarArtists )
    {
        this.m_plugin.configuration.setSimilarArtists( this.m_value, this.m_values );
    } else
    if ( this.m_mode == Configuration.SimilarGenres )
    {
        this.m_plugin.configuration.setSimilarGenres( this.m_value, this.m_values );
    }
}

SimilarForm.prototype.doCancel = function()
{
    this.m_configForm.close();
}

SimilarForm.prototype.applyFilter = function( filter )
{
    var re = new RegExp( filter );
    for ( var i = 0; i < this.m_configForm.values.count; i++ )
    {
        var item = this.m_configForm.values.item( i );
        item.setHidden( !( re.test( item.text() ) ) );
    }
}
