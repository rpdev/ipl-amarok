/*
 *    Copyright (C) 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

Importer.include( "ExplicitSimilarityListEntry.js" );

/**
 * Creates a new ExplicitSimilarityList.
 * This class is used to search in the list of explicitly set similar
 * values.
 * Plugin is the global plugin class. Source is the source value the search
 * starts with. Destination is the node we are looking for.
 * Mode is used as a internal switch to decide on which values we
 * operate. This can be
 *  - artist
 *  - genre
 */
function ExplicitSimilarityList( plugin, source, destination, mode )
{
    this.m_plugin = plugin;
    this.m_source = source;
    this.m_destination = destination;
    this.m_mode = mode;

    this.m_entries = new Array();
}

/**
 * Solves the problem.
 * This will search the tree starting with the source node as root, searching
 * for the given destination root.
 **/
ExplicitSimilarityList.prototype.solve = function()
{
    return this.appendEntry( this.m_source, 1.0, 0.1 );
}





// PRIVATE METHODS

/**
 * Returns a list of similar values for entry.
 * The result depends on the mode, set in the constructor.
 */
ExplicitSimilarityList.prototype.similarValues = function( entry )
{
    if ( this.m_mode == "artist" )
    {
        return this.m_plugin.configuration.similarArtists( entry );
    } else
    if ( this.m_mode == "genre" )
    {
        return this.m_plugin.configuration.similarGenres( entry );
    }
    return new Array();
}

/**
 * Insert a entry (recursively).
 * Entry is the value to insert.
 * Similarity is the similarity to assign to this node.
 * This will recursively insert all similar values of entry, too.
 * The resursively added values will get assigned a similarity of similarity - decPerLevel.
 *
 * This returns the similarity of destination ( as set in the constructor ),
 * which can also be zero (if destination is not in the list of similar
 * values).
 *
 * The recursion stops as soon as similarity reaches zero.
 */
ExplicitSimilarityList.prototype.appendEntry = function( entry, similarity, decPerLevel )
{
    if ( entry == this.m_destination )
    {
        return similarity;
    }
    
    var sentry = new ExplicitSimilarityListEntry( entry, similarity );
    this.m_entries.push( sentry );
    
    var queue = new Array();
    this.enqueue( queue, sentry.similarEntries( this, decPerLevel ) );
    
    while ( queue.length > 0 )
    {
        var current = queue.shift();
        if ( current.entry() == this.m_destination )
        {
            return current.similarity();
        }
        if ( !( this.isInList( current.entry() ) ) )
        {
            this.m_entries.push( current );
            this.enqueue( queue, current.similarEntries( this, decPerLevel ) );
        }
    }
    
    return 0.0;
}

/**
 * Check, whether a given entry is already in the list of similar values.
 * If so, true is returned, otherwise false.
 */
ExplicitSimilarityList.prototype.isInList = function( entry )
{
    for ( var i = 0; i < this.m_entries.length; i++ )
    {
        if ( this.m_entries[ i ].entry() == entry )
        {
            return true;
        }
    }
    return false;
}

/*
 * Adds all entries in items to queue.
 */
ExplicitSimilarityList.prototype.enqueue = function( queue, items )
{
    for ( var i = 0; i < items.length; i++ )
    {
        queue.push( items[ i ] );
    }
}