/*
 *    Copyright (C) 2009, 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

Importer.include( "Track.js" );

/*
 * Constructs a new Collection object.
 * The Collection class provides some functionality to query the Collection,
 * returning our own Track data type.
 */
function Collection( plugin )
{
    Amarok.debug( "Creating new Collection object." );
    this.m_plugin = plugin;
    this.m_ids = new Array();
}

/*
 * Returns a list with random tracks from the collection.
 * This uses the randomTrack method to create a list with numTracks
 * random tracks.
 * It is not guaranteed that the list contains only distinct tracks.
 * If the collection is empty, null is returned.
 */
Collection.prototype.randomTracks = function( numTracks )
{
    var result = new Array();
    while ( numTracks > 0 )
    {
        var track = this.randomTrack();
        if ( track == null )
        {
            return null;
        }
        result.push( track );
        numTracks--;
    }
    return result;
}

/*
 * Select a random track from the collection.
 * This function iterates over the collection, i.e. it fetches a
 * list of all track ids and subsequent calls will select one id,
 * remove it from the list and return a track with this id.
 * Once the list is empty, 
 */
Collection.prototype.randomTrack = function()
{
    if ( this.m_ids.length == 0 )
    {
        this.m_ids = Amarok.Collection.query( "SELECT id FROM tracks;" );
        if ( this.m_ids.length == 0 )
        {
            return null;
        }
    }
    var idx = parseInt( Math.random() * this.m_ids.length, 10 );
    var id = this.m_ids.splice( idx, 1 )[ 0 ];
    var track = new Track( id );
    Amarok.debug( "Selected random track: " + track.prettyName() );
    return track;
}

/*
 * Returns a list with all artists in the collection.
 */
Collection.prototype.artists = function()
{
    return Amarok.Collection.query( "SELECT name FROM artists WHERE name IS NOT NULL AND name != '';" );
}

/*
 * Returns a list with all genres in the collection.
 */
Collection.prototype.genres = function()
{
    return Amarok.Collection.query( "SELECT name FROM genres WHERE name IS NOT NULL AND name != '';" );
}
