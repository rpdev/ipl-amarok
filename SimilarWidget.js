/*
 *    Copyright (C) 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


Importer.loadQtBinding( "qt.core" );
Importer.loadQtBinding( "qt.gui" );
Importer.loadQtBinding( "qt.uitools" );


// PUBLIC

function SimilarWidget( plugin )
{
    this.m_plugin = plugin;

    this.m_artists = null;
    this.m_genres = null;

    // Load UI
    var uiLoader = new QUiLoader( this );
    var uifile = new QFile( Amarok.Info.scriptPath() + "/similarplasmoid.ui" );
    uifile.open( QIODevice.ReadOnly );
    this.m_widget = uiLoader.load( uifile, this );
    uifile.close();

    this.m_widget.approveArtistsLabel[ 'linkActivated(QString)' ].connect( this, this.linkClicked );
    this.m_widget.disapproveArtistsLabel[ 'linkActivated(QString)' ].connect( this, this.linkClicked );
    this.m_widget.approveGenresLabel[ 'linkActivated(QString)' ].connect( this, this.linkClicked );
    this.m_widget.disapproveGenresLabel[ 'linkActivated(QString)' ].connect( this, this.linkClicked );
}

SimilarWidget.prototype.update = function()
{
    this.m_artists = this.m_plugin.playlist.nextSimilarArtists();
    this.m_genres = this.m_plugin.playlist.nextSimilarGenres();

    if ( this.m_artists == null )
    {
        this.m_widget.similarArtistsLabel.text = qsTranslate( "similarwidget", "<i>No more possible similar artists found...</i>" );
        this.m_widget.approveArtistsLabel.hide();
        this.m_widget.disapproveArtistsLabel.hide();
    } else
    {
        this.m_widget.similarArtistsLabel.text = qsTranslate( "similarwidget", "<b>%1</b> and <b>%2</b>" ).arg( this.m_artists[ 0 ] ).arg( this.m_artists[ 1 ] );
        this.m_widget.approveArtistsLabel.show();
        this.m_widget.disapproveArtistsLabel.show();
    }

    if ( this.m_genres == null )
    {
        this.m_widget.similarGenresLabel.text = qsTranslate( "similarwidget", "<i>No more possible similar genres found...</i>" );
        this.m_widget.approveGenresLabel.hide();
        this.m_widget.disapproveGenresLabel.hide();
    } else
    {
        this.m_widget.similarGenresLabel.text = qsTranslate( "similarwidget", "<b>%1</b> and <b>%2</b>" ).arg( this.m_genres[ 0 ] ).arg( this.m_genres[ 1 ] );
        this.m_widget.approveGenresLabel.show();
        this.m_widget.disapproveGenresLabel.show();
    }
}

SimilarWidget.prototype.show = function()
{
    this.update();
    this.m_widget.show();
}

SimilarWidget.prototype.linkClicked = function( url )
{
    if ( url == "addToSimilarArtists" )
    {
        this.m_plugin.playlist.removeSimilarArtists( this.m_artists[ 0 ], this.m_artists[ 1 ], true );
    } else if ( url == "removeFromSimilarArtists" )
    {
        this.m_plugin.playlist.removeSimilarArtists( this.m_artists[ 0 ], this.m_artists[ 1 ], false );
    } else if ( url == "addToSimilarGenres" )
    {
        this.m_plugin.playlist.removeSimilarGenres( this.m_genres[ 0 ], this.m_genres[ 1 ], true );
    } else if ( url == "removeFromSimilarGenres" )
    {
        this.m_plugin.playlist.removeSimilarGenres( this.m_genres[ 0 ], this.m_genres[ 1 ], false );
    }
    this.update();
}
