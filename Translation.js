/*
 *    Copyright (C) 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

Importer.loadQtBinding( "qt.core" );

/*
 * Enable translation of strings in the script.
 * This installs a custom translator in the core application that
 * allows the script to translate its strings.
 *
 * Script files should use the qsTranslate function when handling
 * strings that are displayed to the user.
 *
 * The function takes a context (which can be used to distinguish
 * several strings when their translations into other languages
 * might differ depending on context) and the string that should
 * be translated.
 *
 * The idea is borrowed from the authors of the Ultimate Lyrics
 * script. Thanks to them ;)
 */

function qsTranslate( context, string )
{
    return QCoreApplication.translate( context, string );
}

function installTranslator()
{
    script_translator = new QTranslator();
    script_translator.load( "locale." + QLocale.system().name(),
                            Amarok.Info.scriptPath() + "/translations" );
    QCoreApplication.installTranslator( script_translator );
}