/*
 *    Copyright (C) 2009, 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

Importer.loadQtBinding( "qt.core" );
Importer.loadQtBinding( "qt.gui" );
Importer.loadQtBinding( "qt.uitools" );

Importer.include( "SimilarForm.js" );



// PUBLIC

Configuration.LevenshteinDistance   = 0;
Configuration.SharedWords           = 1;

Configuration.SimilarArtists = "artist";
Configuration.SimilarGenres = "genre";

function Configuration( plugin )
{
    Amarok.debug( "Creating new Configuration object" );

    this.m_plugin = plugin;

    // Initialize properties
    // General
    this.m_enabled              = false;
    this.m_minimalSimilarity    = 0.5;
    this.m_maximalLoops         = 100;
    this.m_upcomingTracks       = 10;
    this.m_playedTracks         = 5;
    this.m_learningEnabled      = true;
    this.m_cacheSize            = 10;
    this.m_matchCompleteList    = true;
    // Attributes
    this.m_trackNamesEnabled    = true;
    this.m_artistsEnabled       = true;
    this.m_albumsEnabled        = true;
    this.m_composersEnabled     = true;
    this.m_yearsEnabled         = true;
    this.m_scoresEnabled        = true;
    this.m_ratingsEnabled       = true;
    this.m_genresEnabled        = true;
    this.m_moodbarsEnabled      = true;
    this.m_honorGroups          = true;
    this.m_honorExplicits       = false;
    // Numbers
    this.m_yearMaxDist          = 50;
    this.m_scoreMaxDist         = 100;
    this.m_ratingMaxDist        = 10;
    // Strings
    this.m_matchTracksStrict    = true;
    this.m_matchArtistsStrict   = true;
    this.m_matchAlbumsStrict    = true;
    this.m_matchComposersStrict = true;
    this.m_matchGenresStrict    = true;
    this.m_fuzzyStringAlgorithm = Configuration.SharedWords;
    this.m_sharedWordsBlacklist = "The the A a An an";
    // Weights
    this.m_tracksWeight         = 0.5;
    this.m_artistsWeight        = 0.5;
    this.m_albumsWeight         = 0.5;
    this.m_composersWeight      = 0.5;
    this.m_genresWeight         = 0.5;
    this.m_yearsWeight          = 0.5;
    this.m_scoresWeight         = 0.5;
    this.m_ratingsWeight        = 0.5;
    this.m_similarArtistsWeight = 0.5;
    this.m_similarGenresWeight  = 0.5;
    this.m_moodbarsWeight       = 0.5;

    // Load UI
    var uiLoader = new QUiLoader( this );
    var uifile = new QFile( Amarok.Info.scriptPath() + "/config.ui" );
    uifile.open( QIODevice.ReadOnly );
    this.m_configForm = uiLoader.load( uifile, this );
    this.m_configForm.tabWidget.setCurrentIndex( 0 );
    uifile.close();
    this.m_configForm.resize( 400, 400 );
    
    this.m_generalTab       = this.m_configForm.tabWidget.widget( 0 );
    this.m_attributesTab    = this.m_configForm.tabWidget.widget( 1 );
    this.m_numberTab        = this.m_configForm.tabWidget.widget( 2 );
    this.m_stringModeTab    = this.m_configForm.tabWidget.widget( 3 );
    this.m_similarTab       = this.m_configForm.tabWidget.widget( 4 );
    this.m_weightTab        = this.m_configForm.tabWidget.widget( 5 );

    this.m_similarArtists   = new Array();
    this.m_similarGenres    = new Array();
    

    // Create form for editing similar artists & genres
    this.m_similarForm = new SimilarForm( plugin );

    // Setup everything
    this.setupConfigurationMenu();
    this.setupToolsMenu();
    this.setupActions();

    // Hint for the "Edit Similar XXX" formular: Save, when called from outside the configuration dialog
    this.m_saveAfterSetSimilarity = false;

    // Load from previous sessions
    this.loadConfig();
    this.saveConfig(); //TODO: Remove ASAP. Just for quick testing, as when s/th goes wrong, Amarok will decently remind us of this incident
}

/*
 * Shows the configuration window.
 */
Configuration.prototype.showConfiguration = function()
{
    try
    {
        Amarok.debug( "Showing Configuration dialog" );
        this.loadConfig();
        // General
        this.m_generalTab.enableIntelligentPlaylist.checked = this.m_enabled;
        this.m_generalTab.maximumLoops.value                = this.m_maximalLoops;
        this.m_generalTab.upcomingTracks.value              = this.m_upcomingTracks;
        this.m_generalTab.playedTracks.value                = this.m_playedTracks;
        this.m_generalTab.learning.checked                  = this.m_learningEnabled;
        this.m_generalTab.cache.value                       = this.m_cacheSize;
        this.setWidgetRelativeValue( this.m_minimalSimilarity, this.m_generalTab.minimalSimilarity );
        this.m_generalTab.matchCompletePlaylist.checked     = this.m_matchCompleteList;
        this.m_generalTab.matchAtLeastOneTrack.checked      = !( this.m_matchCompleteList );
        // Attributes
        this.m_attributesTab.trackEnabled.checked                = this.m_trackNamesEnabled;
        this.m_attributesTab.artistEnabled.checked               = this.m_artistsEnabled;
        this.m_attributesTab.albumEnabled.checked                = this.m_albumsEnabled;
        this.m_attributesTab.composerEnabled.checked             = this.m_composersEnabled;
        this.m_attributesTab.genreEnabled.checked                = this.m_genresEnabled;
        this.m_attributesTab.yearEnabled.checked                 = this.m_yearsEnabled;
        this.m_attributesTab.scoreEnabled.checked                = this.m_scoresEnabled;
        this.m_attributesTab.ratingEnabled.checked               = this.m_ratingsEnabled;
        this.m_attributesTab.moodbarEnabled.checked              = this.m_moodbarsEnabled;
        this.m_attributesTab.preferFilesFromGroup.checked        = this.m_honorGroups;
        this.m_attributesTab.preferExplicitSimilarTracks.checked = this.m_honorExplicits;
        // Numbers
        this.m_numberTab.yearMaxDistance.value      = this.m_yearMaxDist;
        this.m_numberTab.scoreMaxDistance.value     = this.m_scoreMaxDist;
        this.m_numberTab.ratingMaxDistance.value    = this.m_ratingMaxDist;
        // Strings
        this.m_stringModeTab.strictMatchingList.clear();
        this.m_stringModeTab.fuzzyMatchingList.clear();
        this.addItemToStringMatchingList( this.m_attributesTab.trackEnabled,    this.m_matchTracksStrict );
        this.addItemToStringMatchingList( this.m_attributesTab.artistEnabled,   this.m_matchArtistsStrict );
        this.addItemToStringMatchingList( this.m_attributesTab.albumEnabled,    this.m_matchAlbumsStrict );
        this.addItemToStringMatchingList( this.m_attributesTab.composerEnabled, this.m_matchComposersStrict );
        this.addItemToStringMatchingList( this.m_attributesTab.genreEnabled,    this.m_matchGenresStrict );
        this.m_stringModeTab.stringAlgorithmBox.levenshteinDistance.checked    = this.m_fuzzyStringAlgorithm == Configuration.LevenshteinDistance;
        this.m_stringModeTab.stringAlgorithmBox.sharedWords.checked            = this.m_fuzzyStringAlgorithm == Configuration.SharedWords;
        this.m_stringModeTab.stringAlgorithmBox.wordBlacklist.text             = this.m_sharedWordsBlacklist;
        // Explicit Similarity
        this.updateSimilarityTables();
        // Weights
        this.setWidgetRelativeValue( this.m_tracksWeight,         this.m_weightTab.trackWeight );
        this.setWidgetRelativeValue( this.m_artistsWeight,        this.m_weightTab.artistWeight );
        this.setWidgetRelativeValue( this.m_albumsWeight,         this.m_weightTab.albumWeight );
        this.setWidgetRelativeValue( this.m_composersWeight,      this.m_weightTab.composerWeight );
        this.setWidgetRelativeValue( this.m_genresWeight,         this.m_weightTab.genreWeight );
        this.setWidgetRelativeValue( this.m_yearsWeight,          this.m_weightTab.yearWeight );
        this.setWidgetRelativeValue( this.m_scoresWeight,         this.m_weightTab.scoreWeight );
        this.setWidgetRelativeValue( this.m_ratingsWeight,        this.m_weightTab.ratingWeight );
        this.setWidgetRelativeValue( this.m_similarArtistsWeight, this.m_weightTab.similarArtistsWeight );
        this.setWidgetRelativeValue( this.m_similarGenresWeight,  this.m_weightTab.similarGenresWeight );
        this.setWidgetRelativeValue( this.m_moodbarsWeight,       this.m_weightTab.moodbarWeight );


        this.m_configForm.show();
    } catch ( ex )
    {
        printStackTrace( ex );
    }
}

 // GETTER // 
Configuration.prototype.includeTrack = function()
{
    return this.m_trackNamesEnabled;
}

Configuration.prototype.includeArtist = function()
{
    return this.m_artistsEnabled;
}

Configuration.prototype.includeAlbum = function()
{
    return this.m_albumsEnabled;
}

Configuration.prototype.includeComposer = function()
{
    return this.m_composersEnabled;
}

Configuration.prototype.includeGenre = function()
{
    return this.m_genresEnabled;
}

Configuration.prototype.includeYear = function()
{
    return this.m_yearsEnabled;
}

Configuration.prototype.includeScore = function()
{
    return this.m_scoresEnabled;
}

Configuration.prototype.includeRating = function()
{
    return this.m_ratingsEnabled;
}

Configuration.prototype.includeMoodbar = function()
{
    return this.m_moodbarsEnabled;
}

Configuration.prototype.honorGroups = function()
{
    return this.m_honorGroups;
}

Configuration.prototype.honorExplicits = function()
{
    return this.m_honorExplicits;
}

Configuration.prototype.trackWeight = function()
{
    return this.m_tracksWeight;
}

Configuration.prototype.artistWeight = function()
{
    return this.m_artistsWeight;
}

Configuration.prototype.similarArtistsWeight = function()
{
    return this.m_similarArtistsWeight;
}

Configuration.prototype.albumWeight = function()
{
    return this.m_albumsWeight;
}

Configuration.prototype.composerWeight = function()
{
    return this.m_composersWeight;
}

Configuration.prototype.genreWeight = function()
{
    return this.m_genresWeight;
}

Configuration.prototype.similarGenresWeight = function()
{
    return this.m_similarGenresWeight;
}

Configuration.prototype.yearWeight = function()
{
    return this.m_yearsWeight;
}

Configuration.prototype.scoreWeight = function()
{
    return this.m_scoresWeight;
}

Configuration.prototype.ratingWeight = function()
{
    return this.m_ratingsWeight;
}

Configuration.prototype.moodbarWeight = function()
{
    return this.m_moodbarsWeight;
}

Configuration.prototype.adjustTrackWeight = function( val )
{
    this.m_tracksWeight = this.fitWeight( this.m_tracksWeight + val / this.m_weightTab.trackWeight.maximum );
}

Configuration.prototype.adjustArtistWeight = function( val )
{
    this.m_artistsWeight = this.fitWeight( this.m_artistsWeight + val / this.m_weightTab.artistWeight.maximum );
}

Configuration.prototype.adjustAlbumWeight = function( val )
{
    this.m_albumsWeight = this.fitWeight( this.m_albumsWeight + val / this.m_weightTab.albumWeight.maximum );
}

Configuration.prototype.adjustComposerWeight = function( val )
{
    this.m_composersWeight = this.fitWeight( this.m_composersWeight + val / this.m_weightTab.composerWeight.maximum );
}

Configuration.prototype.adjustGenreWeight = function( val )
{
    this.m_genresWeight = this.fitWeight( this.m_genresWeight + val / this.m_weightTab.genreWeight.maximum );
}

Configuration.prototype.adjustYearWeight = function( val )
{
    this.m_yearsWeight = this.fitWeight( this.m_yearsWeight + val / this.m_weightTab.yearWeight.maximum );
}

Configuration.prototype.adjustScoreWeight = function( val )
{
    this.m_scoresWeight = this.fitWeight( this.m_scoresWeight + val / this.m_weightTab.scoreWeight.maximum );
}

Configuration.prototype.adjustRatingWeight = function( val )
{
    this.m_ratingsWeight = this.fitWeight( this.m_ratingsWeight + val / this.m_weightTab.ratingWeight.maximum );
}

Configuration.prototype.adjustMoodbarWeight = function( val )
{
    this.m_moodbarsWeight = this.fitWeight( this.m_moodbarsWeight + val / this.m_weightTab.moodbarWeight.maximum );
}

Configuration.prototype.adjustSimilarArtistWeight = function( val )
{
    this.m_similarArtistsWeight = this.fitWeight( this.m_similarArtistsWeight + val / this.m_weightTab.similarArtistsWeight.maximum );
}

Configuration.prototype.adjustSimilarGenreWeight = function( val )
{
    this.m_similarGenresWeight = this.fitWeight( this.m_similarGenresWeight + val / this.m_weightTab.similarGenresWeight.maximum );
}

Configuration.prototype.matchTrackStrict = function()
{
    return this.m_matchTracksStrict;
}

Configuration.prototype.matchArtistStrict = function()
{
    return this.m_matchArtistsStrict;
}

Configuration.prototype.matchAlbumStrict = function()
{
    return this.m_matchAlbumsStrict;
}

Configuration.prototype.matchComposerStrict = function()
{
    return this.m_matchComposersStrict;
}

Configuration.prototype.matchGenreStrict = function()
{
    return this.m_matchGenresStrict;
}

Configuration.prototype.fuzzyStringMatchingAlgorithm = function()
{
    return this.m_fuzzyStringAlgorithm;
}

Configuration.prototype.sharedWordsBlacklist = function()
{
    return this.m_sharedWordsBlacklist.split( " " );
}

Configuration.prototype.yearMaxDistance = function()
{
    return this.m_yearMaxDist;
}

Configuration.prototype.scoreMaxDistance = function()
{
    return this.m_scoreMaxDist;
}

Configuration.prototype.ratingMaxDistance = function()
{
    return this.m_ratingMaxDist;
}

Configuration.prototype.minimalSimilarity = function()
{
    return this.m_minimalSimilarity;
}

Configuration.prototype.setMinimalSimilarity = function( ms )
{
    this.m_minimalSimilarity = ms;
    this.saveConfig();
}

Configuration.prototype.intelligentPlaylistEnabled = function()
{
    return this.m_enabled;
}

Configuration.prototype.maximumLoops = function()
{
    return this.m_maximalLoops;
}

Configuration.prototype.learningEnabled = function()
{
    return this.m_learningEnabled;
}

Configuration.prototype.upcomingTracks = function()
{
    return this.m_upcomingTracks;
}

Configuration.prototype.playedTracks = function()
{
    return this.m_playedTracks;
}

Configuration.prototype.cacheSize = function()
{
    return this.m_cacheSize;
}

Configuration.prototype.cacheMaxTTL = function()
{
    return this.playedTracks() + this.upcomingTracks();
}

Configuration.prototype.matchCompletePlaylist = function()
{
    return this.m_matchCompleteList;
}

Configuration.prototype.setSimilarArtists = function( artist, artists )
{
    this.setSimilarityEntry( this.m_similarArtists, artist, artists );
}

Configuration.prototype.setSimilarGenres = function( genre, genres )
{
    this.setSimilarityEntry( this.m_similarGenres, genre, genres );
}

Configuration.prototype.setSimilarArtistsAndSave = function( artist, artists )
{
    this.setSimilarityEntry( this.m_similarArtists, artist, artists );
    this.saveSimilarityList( "explicitSimilarArtists",  this.m_similarArtists );
}

Configuration.prototype.setSimilarGenresAndSave = function( genre, genres )
{
    this.setSimilarityEntry( this.m_similarGenres, genre, genres );
    this.saveSimilarityList( "explicitSimilarGenres",   this.m_similarGenres );
}

Configuration.prototype.similarArtists = function( artist )
{
    return this.similarityEntry( this.m_similarArtists, artist );
}

Configuration.prototype.similarGenres = function( genre )
{
    return this.similarityEntry( this.m_similarGenres, genre );
}

Configuration.prototype.areSimilarArtists = function( artist1, artist2 )
{
    return this.areSimilar( this.m_similarArtists, artist1, artist2 );
}

Configuration.prototype.areSimilarGenres = function( genre1, genre2 )
{
    return this.areSimilar( this.m_similarGenres, genre1, genre2 );
}

/*
 * Returns a list of tuples, where each tuple consists of
 * the ID, the (displayable) name and the current weight of
 * an attribute used for track comparison.
 *
 * This can be used for calculating initial weights.
 */
Configuration.prototype.getWeights = function()
{
    var result = new Array();
    result.push( new Array( SimilarityVector.TrackAttribute, qsTranslate( "configuration", "Track Name" ), this.trackWeight() ) );
    result.push( new Array( SimilarityVector.ArtistAttribute, qsTranslate( "configuration", "Artist Name" ), this.artistWeight() ) );
    result.push( new Array( SimilarityVector.AlbumAttribute, qsTranslate( "configuration", "Album Name" ), this.albumWeight() ) );
    result.push( new Array( SimilarityVector.ComposerAttribute, qsTranslate( "configuration", "Composer Name" ), this.composerWeight() ) );
    result.push( new Array( SimilarityVector.GenreAttribute, qsTranslate( "configuration", "Genre Name" ), this.genreWeight() ) );
    result.push( new Array( SimilarityVector.YearAttribute, qsTranslate( "configuration", "Year" ), this.yearWeight() ) );
    result.push( new Array( SimilarityVector.ScoreAttribute, qsTranslate( "configuration", "Score" ), this.scoreWeight() ) );
    result.push( new Array( SimilarityVector.RatingAttribute, qsTranslate( "configuration", "Rating" ), this.ratingWeight() ) );
    result.push( new Array( SimilarityVector.SimilarArtistsAttribute, qsTranslate( "configuration", "Similar Artists" ), this.similarArtistsWeight() ) );
    result.push( new Array( SimilarityVector.SimilarGenresAttribute, qsTranslate( "configuration", "Similar Genres" ), this.similarGenresWeight() ) );
    result.push( new Array( SimilarityVector.MoodbarAttribute, qsTranslate( "configuration", "Moodbar" ), this.moodbarWeight() ) );
    return result;
}

Configuration.prototype.setWeights = function( weights )
{
    for ( var i = 0; i < weights.length; i++ )
    {
        switch ( weights[ i ][ 0 ] )
        {
            case SimilarityVector.TrackAttribute:           this.m_tracksWeight         = weights[ i ][ 2 ]; break;
            case SimilarityVector.ArtistAttribute:          this.m_artistsWeight        = weights[ i ][ 2 ]; break;
            case SimilarityVector.AlbumAttribute:           this.m_albumsWeight         = weights[ i ][ 2 ]; break;
            case SimilarityVector.ComposerAttribute:        this.m_composersWeight      = weights[ i ][ 2 ]; break;
            case SimilarityVector.GenreAttribute:           this.m_genresWeight         = weights[ i ][ 2 ]; break;
            case SimilarityVector.YearAttribute:            this.m_yearsWeight          = weights[ i ][ 2 ]; break;
            case SimilarityVector.ScoreAttribute:           this.m_scoresWeight         = weights[ i ][ 2 ]; break;
            case SimilarityVector.RatingAttribute:          this.m_ratingsWeight        = weights[ i ][ 2 ]; break;
            case SimilarityVector.SimilarArtistsAttribute:  this.m_similarArtistsWeight = weights[ i ][ 2 ]; break;
            case SimilarityVector.SimilarGenresAttribute:   this.m_similarGenresWeight  = weights[ i ][ 2 ]; break;
            case SimilarityVector.MoodbarAttribute:         this.m_moodbarsWeight       = weights[ i ][ 2 ]; break;
        }
    }
    this.saveConfig();
}





// PRIVATE

/*
 * Check, if v1 and v2 is a similar pair, i.e. if there is an edge between
 * the vertices v1 and v2 in the graph given by list.
 */
Configuration.prototype.isSimilarPair = function( list, v1, v2 )
{
    if ( v1 == v2 )
    {
        return true;
    }
    for ( var i = 0; i < list.length; i++ )
    {
        if ( ( list[ i ][ 0 ] == v1 && list[ i ][ 1 ] == v2 ) ||
             ( list[ i ][ 0 ] == v2 && list[ i ][ 1 ] == v1 ) )
        {
            return true;
        }
    }
    return false;
}

/*
 * Adds similar pairs, i.e. adds an edge between key and every entry in value
 * in the graph given by list.
 */
Configuration.prototype.setSimilarityEntry = function( list, key, value )
{
    for ( var i = 0; i < value.length; i++ )
    {
        if ( !( this.isSimilarPair( list, key, value[ i ] ) ) )
        {
            list.push( new Array( key, value[ i ] ) );
        }
    }

    if ( this.m_saveAfterSetSimilarity )
    {
        this.saveConfig();
        this.m_saveAfterSetSimilarity = false;
    }
    
    this.updateSimilarityTables();
}

/*
 * Returns a list of all similar entries belonging to key, i.e.
 * a list of all vertices key is connected to in the graph
 * given by list.
 */
Configuration.prototype.similarityEntry = function( list, key )
{
    var result = new Array();
    for ( var i = 0; i < list.length; i++ )
    {
        if ( list[ i ][ 0 ] == key )
        {
            result.push( list[ i ][ 1 ] );
        } else if ( list[ i ][ 1 ] == key )
        {
            result.push( list[ i ][ 0 ] );
        }
    }
    return result;
}

/*
 * Test, if v1 and v2 are similar, i.e. if they are in the relation given by list.
 */
Configuration.prototype.areSimilar = function( list, v1, v2 )
{
    if ( v1 == v2 )
    {
        return true;
    }
    for ( var i = 0; i < list.length; i++ )
    {
        if ( ( list[ i ][ 0 ] == v1 && list[ i ][ 1 ] == v2 ) ||
             ( list[ i ][ 0 ] == v2 && list[ i ][ 1 ] == v1 ) )
        {
            return true;
        }
    }
    return false;
}

/*
 * Update the similarity tables in the GUI.
 */
Configuration.prototype.updateSimilarityTables = function()
{
    try
    {
        // Set up the table showing similar artists
        var similarArtistsTable = this.m_similarTab.similarArtistsTable;
        var artists = this.m_plugin.collection.artists();
        similarArtistsTable.rowCount = artists.length;
        for ( var i = 0; i < artists.length; i++ )
        {
            var key = new QTableWidgetItem( artists[ i ] );
            var value = new QTableWidgetItem( this.similarArtists( artists[ i ] ).join( ", " ) );
            similarArtistsTable.setItem( i, 0, key );
            similarArtistsTable.setItem( i, 1, value );
        }

        // Set up the table showing similar genres
        var similarGenresTable = this.m_similarTab.similarGenresTable;
        var genres = this.m_plugin.collection.genres();
        similarGenresTable.rowCount = genres.length;
        for ( var i = 0; i < genres.length; i++ )
        {
            var key = new QTableWidgetItem( genres[ i ] );
            var value = new QTableWidgetItem( this.similarGenres( genres[ i ] ).join( ", " ) );
            similarGenresTable.setItem( i, 0, key );
            similarGenresTable.setItem( i, 1, value );
        }
    } catch ( ex )
    {
        printStackTrace( ex );
    }
}

/*
 * Edit the similar artists for the artist selected in the table.
 */
Configuration.prototype.editSimilarArtists = function()
{
    try
    {
        var similarArtistsTable = this.m_similarTab.similarArtistsTable;
        var item = similarArtistsTable.item( similarArtistsTable.currentRow(), 0 );
        if ( item != null && item.text().length > 0 )
        {
            this.m_similarForm.show( Configuration.SimilarArtists,
                                    item.text(), this.similarArtists( item.text() ),
                                    this.m_plugin.collection.artists() );
        }
    } catch ( ex )
    {
        printStackTrace( ex );
    }
}

/*
 * Edit the similar artists for the artist selected in the table.
 * This is an extended version of editSimilarArtists(), which will
 * immediately save the list after closing the dialog.
 */
Configuration.prototype.editSimilarArtistsAndSave = function()
{
    try
    {
        var currentTrack = this.m_plugin.playlist.currentTrack();
        if ( currentTrack.artist() && currentTrack.artist().length > 0 )
        {
            this.m_saveAfterSetSimilarity = true;
            this.m_similarForm.show( Configuration.SimilarArtists,
                                     currentTrack.artist(),
                                     this.similarArtists( currentTrack.artist() ),
                                     this.m_plugin.collection.artists() );
        }
    } catch ( ex )
    {
        printStackTrace( ex );
    }
}

/*
 * Edit the similar genres for the genre selected in the table.
 */
Configuration.prototype.editSimilarGenres = function()
{
    try
    {
        var similarGenresTable = this.m_similarTab.similarGenresTable;
        var item = similarGenresTable.item( similarGenresTable.currentRow(), 0 );
        if ( item != null && item.text().length > 0 )
        {
            this.m_similarForm.show( Configuration.SimilarGenres,
                                    item.text(), this.similarGenres( item.text() ),
                                    this.m_plugin.collection.genres() );
        }
    } catch ( ex )
    {
        printStackTrace( ex );
    }
}

/*
 * Edit the similar genres for the genre selected in the table.
 * This is an extended version of editSimilarGenres(), which will
 * save the list immediately after closing the dialog.
 */
Configuration.prototype.editSimilarGenresAndSave = function()
{
    try
    {
        var currentTrack = this.m_plugin.playlist.currentTrack();
        if ( currentTrack.genre() && currentTrack.genre().length > 0 )
        {
            this.m_saveAfterSetSimilarity = true;
            this.m_similarForm.show( Configuration.SimilarGenres,
                                     currentTrack.genre(),
                                     this.similarGenres( currentTrack.genre() ),
                                     this.m_plugin.collection.genres() );
        }
    } catch ( ex )
    {
        printStackTrace( ex );
    }
}

/*
 * Read back the settings from GUI and save.
 */
Configuration.prototype.onAccept = function()
{
    try
    {
        Amarok.debug( "Applying Configuration settings" );
        // General
        this.m_enabled              = this.m_generalTab.enableIntelligentPlaylist.checked;
        this.m_minimalSimilarity    = this.widgetRelativeValue( this.m_generalTab.minimalSimilarity );
        this.m_maximalLoops         = this.m_generalTab.maximumLoops.value;
        this.m_upcomingTracks       = this.m_generalTab.upcomingTracks.value;
        this.m_playedTracks         = this.m_generalTab.playedTracks.value;
        this.m_learningEnabled      = this.m_generalTab.learning.checked;
        this.m_cacheSize            = this.m_generalTab.cache.value;
        this.m_matchCompleteList    = this.m_generalTab.matchCompletePlaylist.checked;
        // Attributes
        this.m_trackNamesEnabled    = this.m_attributesTab.trackEnabled.checked;
        this.m_artistsEnabled       = this.m_attributesTab.artistEnabled.checked;
        this.m_albumsEnabled        = this.m_attributesTab.albumEnabled.checked;
        this.m_composersEnabled     = this.m_attributesTab.composerEnabled.checked;
        this.m_genresEnabled        = this.m_attributesTab.genreEnabled.checked;
        this.m_yearsEnabled         = this.m_attributesTab.yearEnabled.checked;
        this.m_scoresEnabled        = this.m_attributesTab.scoreEnabled.checked;
        this.m_ratingsEnabled       = this.m_attributesTab.ratingEnabled.checked;
        this.m_moodbarsEnabled      = this.m_attributesTab.moodbarEnabled.checked;
        this.m_honorGroups          = this.m_attributesTab.preferFilesFromGroup.checked;
        this.m_honorExplicits       = this.m_attributesTab.preferExplicitSimilarTracks.checked;
        // Numbers
        this.m_yearMaxDist      = this.m_numberTab.yearMaxDistance.value;
        this.m_scoreMaxDist     = this.m_numberTab.scoreMaxDistance.value;
        this.m_ratingMaxDist    = this.m_numberTab.ratingMaxDistance.value;
        // Strings
        this.m_matchTracksStrict    = this.getStringMatchingMode( this.m_attributesTab.trackEnabled );
        this.m_matchArtistsStrict   = this.getStringMatchingMode( this.m_attributesTab.artistEnabled );
        this.m_matchAlbumsStrict    = this.getStringMatchingMode( this.m_attributesTab.albumEnabled );
        this.m_matchComposersStrict = this.getStringMatchingMode( this.m_attributesTab.composerEnabled );
        this.m_matchGenresStrict    = this.getStringMatchingMode( this.m_attributesTab.genreEnabled );
        this.m_fuzzyStringAlgorithm = Configuration.LevenshteinDistance;
        if ( this.m_stringModeTab.stringAlgorithmBox.sharedWords.checked )
        {
            this.m_fuzzyStringAlgorithm = Configuration.SharedWords;
        }
        this.m_sharedWordsBlacklist = this.m_stringModeTab.stringAlgorithmBox.wordBlacklist.text;
        // Explicit Similarity
        this.updateSimilarityTables();
        // Weights
        this.m_tracksWeight         = this.widgetRelativeValue( this.m_weightTab.trackWeight );
        this.m_artistsWeight        = this.widgetRelativeValue( this.m_weightTab.artistWeight );
        this.m_albumsWeight         = this.widgetRelativeValue( this.m_weightTab.albumWeight );
        this.m_composersWeight      = this.widgetRelativeValue( this.m_weightTab.composerWeight );
        this.m_genresWeight         = this.widgetRelativeValue( this.m_weightTab.genreWeight );
        this.m_yearsWeight          = this.widgetRelativeValue( this.m_weightTab.yearWeight );
        this.m_scoresWeight         = this.widgetRelativeValue( this.m_weightTab.scoreWeight );
        this.m_ratingsWeight        = this.widgetRelativeValue( this.m_weightTab.ratingWeight );
        this.m_similarArtistsWeight = this.widgetRelativeValue( this.m_weightTab.similarArtistsWeight );
        this.m_similarGenresWeight  = this.widgetRelativeValue( this.m_weightTab.similarGenresWeight );
        this.m_moodbarsWeight       = this.widgetRelativeValue( this.m_weightTab.moodbarWeight );

        this.saveConfig();
        this.m_configForm.close();
    } catch ( ex )
    {
        printStackTrace( ex );
    }
}

/*
 * Restore old settings and close the dialog.
 */
Configuration.prototype.onReject = function()
{
    this.loadConfig();
    this.m_configForm.close();
}

/*
 * Move the selected entry to the strict matching string table.
 */
Configuration.prototype.onMoveToStrict = function()
{
    this.moveItemsToList( this.m_stringModeTab.fuzzyMatchingList, this.m_stringModeTab.strictMatchingList );
}

/*
 * Move the selected entry to the fuzzy matching string table.
 */
Configuration.prototype.onMoveToFuzzy = function()
{
    this.moveItemsToList( this.m_stringModeTab.strictMatchingList, this.m_stringModeTab.fuzzyMatchingList );
}

/*
 * Move the item selected in from to the list to.
 */
Configuration.prototype.moveItemsToList = function( from, to )
{
    to.addItem( from.currentItem().text() );
    from.takeItem( from.row( from.currentItem() ) );
}

/*
 * Toggle the enabled state of the plugin.
 * This is used from the application menu
 * as a shortcut for the user.
 */
Configuration.prototype.onChangeEnabledState = function( checked )
{
    this.m_enabled = checked;
    this.saveConfig();
}

// Several loader and saver methods

Configuration.prototype.loadBoolean = function( key, def )
{
    var bs = "false";
    if ( def )
    {
        bs = "true";
    }
    return ( Amarok.Script.readConfig( key, bs ) == "true" );
}

Configuration.prototype.saveBoolean = function( key, value )
{
    if ( value )
    {
        Amarok.Script.writeConfig( key, "true" );
    } else
    {
        Amarok.Script.writeConfig( key, "false" );
    }
}

Configuration.prototype.loadInt = function( key, def )
{
    return parseInt( Amarok.Script.readConfig( key, "" + parseInt( def, 10 ) ) );
}

Configuration.prototype.saveInt = function( key, value )
{
    Amarok.Script.writeConfig( key, "" + parseInt( value, 10 ) );
}

Configuration.prototype.loadFloat = function( key, def )
{
    return parseFloat( Amarok.Script.readConfig( key, "" + parseFloat( def ) ) );
}

Configuration.prototype.saveFloat = function( key, value )
{
    Amarok.Script.writeConfig( key, "" + parseFloat( value ) );
}

Configuration.prototype.loadString = function( key, def )
{
    return Amarok.Script.readConfig( key, "" + def );
}

Configuration.prototype.saveString = function( key, value )
{
    Amarok.Script.writeConfig( key, "" + value );
}

Configuration.prototype.loadSimilarityList = function( key )
{
    var result = new Array();
    var s = Amarok.Script.readConfig( key, "" );
    var entries = s.split( ";;;" );
    for ( var numEntry = 0; numEntry < entries.length; numEntry++ )
    {
        var kv = entries[ numEntry ].split( "===" );
        if ( kv.length >= 2 )
        {
            result.push( new Array( kv[ 0 ], kv[ 1 ] ) );
        }
    }
    return result;
}

Configuration.prototype.saveSimilarityList = function( key, list )
{
    //TODO: Do not save entries that are no longer in our DB?
    var s = "";
    for ( i = 0; i < list.length; i++ )
    {
        if ( s.length > 0 )
        {
            s = s + ";;;";
        }
        s = s + list[ i ].join( "===" );
    }
    Amarok.Script.writeConfig( key, s );
}

/*
 * Load the configuration from disk.
 */
Configuration.prototype.loadConfig = function()
{
    this.m_enabled                  = this.loadBoolean( "m_enabled",            false );
    this.m_minimalSimilarity        = this.loadFloat(   "m_minimalSimilarity",  0.5 );
    this.m_maximalLoops             = this.loadInt(     "m_maximalLoops",       100 );
    this.m_upcomingTracks           = this.loadInt(     "m_upcomingTracks",     10 );
    this.m_playedTracks             = this.loadInt(     "m_playedTracks",       5 );
    this.m_learningEnabled          = this.loadBoolean( "m_learningEnabled",    true );
    this.m_cacheSize                = this.loadInt(     "m_cacheSize",          10 );
    this.m_matchCompleteList        = this.loadBoolean( "m_matchCompleteList",  true );

    this.m_trackNamesEnabled    = this.loadBoolean( "m_trackNamesEnabled",      true );
    this.m_artistsEnabled       = this.loadBoolean( "m_artistsEnabled",         true );
    this.m_albumsEnabled        = this.loadBoolean( "m_albumsEnabled",          true );
    this.m_composersEnabled     = this.loadBoolean( "m_composersEnabled",       true );
    this.m_yearsEnabled         = this.loadBoolean( "m_yearsEnabled",           true );
    this.m_scoresEnabled        = this.loadBoolean( "m_scoresEnabled",          true );
    this.m_ratingsEnabled       = this.loadBoolean( "m_ratingsEnabled",         true );
    this.m_genresEnabled        = this.loadBoolean( "m_genresEnabled",          true );
    this.m_moodbarsEnabled      = this.loadBoolean( "m_moodbarsEnabled",        true );
    this.m_honorGroups          = this.loadBoolean( "m_honorGroups",            true );
    this.m_honorExplicits       = this.loadBoolean( "m_honorExplicits",         false );

    this.m_yearMaxDist          = this.loadInt( "m_yearMaxDist",    50 );
    this.m_scoreMaxDist         = this.loadInt( "m_scoreMaxDist",   100 );
    this.m_ratingMaxDist        = this.loadInt( "m_ratingMaxDist",  10 );

    this.m_matchTracksStrict    = this.loadBoolean( "m_matchTracksStrict",      true );
    this.m_matchArtistsStrict   = this.loadBoolean( "m_matchArtistsStrict",     true );
    this.m_matchAlbumsStrict    = this.loadBoolean( "m_matchAlbumsStrict",      true );
    this.m_matchComposersStrict = this.loadBoolean( "m_matchComposersStrict",   true );
    this.m_matchGenresStrict    = this.loadBoolean( "m_matchGenresStrict",      true );
    this.m_fuzzyStringAlgorithm = this.loadInt(     "m_fuzzyStringAlgorithm",   Configuration.SharedWords );
    this.m_sharedWordsBlacklist = this.loadString(  "m_sharedWordsBlacklist",   "The the A a An an" );

    this.m_similarArtists   = this.loadSimilarityList( "explicitSimilarArtists" );
    this.m_similarGenres    = this.loadSimilarityList( "explicitSimilarGenres" );

    this.m_tracksWeight         = this.loadFloat( "m_tracksWeight",             0.5 );
    this.m_artistsWeight        = this.loadFloat( "m_artistsWeight",            0.5 );
    this.m_albumsWeight         = this.loadFloat( "m_albumsWeight",             0.5 );
    this.m_composersWeight      = this.loadFloat( "m_composersWeight",          0.5 );
    this.m_genresWeight         = this.loadFloat( "m_genresWeight",             0.5 );
    this.m_yearsWeight          = this.loadFloat( "m_yearsWeight",              0.5 );
    this.m_scoresWeight         = this.loadFloat( "m_scoresWeight",             0.5 );
    this.m_ratingsWeight        = this.loadFloat( "m_ratingsWeight",            0.5 );
    this.m_similarArtistsWeight = this.loadFloat( "m_similarArtistsWeight",     0.5 );
    this.m_similarGenresWeight  = this.loadFloat( "m_similarGenresWeight",      0.5 );
    this.m_moodbarsWeight       = this.loadFloat( "m_moodbarsWeight",           0.5 );

    this.updateGui();
}

/*
 * Save the configuration to disk.
 */
Configuration.prototype.saveConfig = function()
{
    this.saveBoolean( "m_enabled",            this.m_enabled );
    this.saveFloat(   "m_minimalSimilarity",  this.m_minimalSimilarity );
    this.saveInt(     "m_maximalLoops",       this.m_maximalLoops );
    this.saveInt(     "m_upcomingTracks",     this.m_upcomingTracks );
    this.saveInt(     "m_playedTracks",       this.m_playedTracks );
    this.saveBoolean( "m_learningEnabled",    this.m_learningEnabled );
    this.saveInt(     "m_cacheSize",          this.m_cacheSize );
    this.saveBoolean( "m_matchCompleteList",  this.m_matchCompleteList );

    this.saveBoolean( "m_trackNamesEnabled",      this.m_trackNamesEnabled );
    this.saveBoolean( "m_artistsEnabled",         this.m_artistsEnabled );
    this.saveBoolean( "m_albumsEnabled",          this.m_albumsEnabled );
    this.saveBoolean( "m_composersEnabled",       this.m_composersEnabled );
    this.saveBoolean( "m_yearsEnabled",           this.m_yearsEnabled );
    this.saveBoolean( "m_scoresEnabled",          this.m_scoresEnabled );
    this.saveBoolean( "m_ratingsEnabled",         this.m_ratingsEnabled );
    this.saveBoolean( "m_genresEnabled",          this.m_genresEnabled );
    this.saveBoolean( "m_moodbarsEnabled",        this.m_moodbarsEnabled );
    this.saveBoolean( "m_honorGroups",            this.m_honorGroups );
    this.saveBoolean( "m_honorExplicits",         this.m_honorExplicits );

    this.saveInt( "m_yearMaxDist",    this.m_yearMaxDist );
    this.saveInt( "m_scoreMaxDist",   this.m_scoreMaxDist );
    this.saveInt( "m_ratingMaxDist",  this.m_ratingMaxDist );

    this.saveBoolean( "m_matchTracksStrict",      this.m_matchTracksStrict);
    this.saveBoolean( "m_matchArtistsStrict",     this.m_matchArtistsStrict);
    this.saveBoolean( "m_matchAlbumsStrict",      this.m_matchAlbumsStrict);
    this.saveBoolean( "m_matchComposersStrict",   this.m_matchComposersStrict);
    this.saveBoolean( "m_matchGenresStrict",      this.m_matchGenresStrict);
    this.saveInt(     "m_fuzzyStringAlgorithm",   this.m_fuzzyStringAlgorithm );
    this.saveString(  "m_sharedWordsBlacklist",   this.m_sharedWordsBlacklist );

    this.saveSimilarityList( "explicitSimilarArtists",  this.m_similarArtists );
    this.saveSimilarityList( "explicitSimilarGenres",   this.m_similarGenres );

    this.saveFloat( "m_tracksWeight",             this.m_tracksWeight );
    this.saveFloat( "m_artistsWeight",            this.m_artistsWeight );
    this.saveFloat( "m_albumsWeight",             this.m_albumsWeight );
    this.saveFloat( "m_composersWeight",          this.m_composersWeight );
    this.saveFloat( "m_genresWeight",             this.m_genresWeight );
    this.saveFloat( "m_yearsWeight",              this.m_yearsWeight );
    this.saveFloat( "m_scoresWeight",             this.m_scoresWeight );
    this.saveFloat( "m_ratingsWeight",            this.m_ratingsWeight );
    this.saveFloat( "m_similarArtistsWeight",     this.m_similarArtistsWeight );
    this.saveFloat( "m_similarGenresWeight",      this.m_similarGenresWeight );
    this.saveFloat( "m_moodbarsWeight",           this.m_moodbarsWeight );

    this.updateGui();
}

/*
 * Select the list (strict/fuzzy) depending on mode. True selects the
 * strict matching list, false the fuzzy matching list.
 */
Configuration.prototype.selectStringMatchingList = function( mode )
{
    if ( mode )
    {
        return this.m_stringModeTab.strictMatchingList;
    } else
    {
        return this.m_stringModeTab.fuzzyMatchingList;
    }
}

/*
 * Add item to either strict or fuzzy string matching list, depending on mode,
 * where mode means "strict" in case of it's true, otherwise "fuzzy".
 */
Configuration.prototype.addItemToStringMatchingList = function( item, mode )
{
    var text = item.text.replace( "&", "" );
    this.selectStringMatchingList( mode ).addItem( text );
}

/*
 * Read the method item shall be matched in string comparisons.
 */
Configuration.prototype.getStringMatchingMode = function( item )
{
    var text = item.text.replace( "&", "" );
    for ( var i = 0; i < this.m_stringModeTab.strictMatchingList.count; i++ )
    {
        var it = this.m_stringModeTab.strictMatchingList.item( i );
        if ( text == it.text() )
        {
            return true;
        }
    }
    return false;
}

/*
 * Set the slider, given by widget, using a relative value, where rel = 1.0
 * means widget.maximum and rel = 0.0 widget.minimum.
 */
Configuration.prototype.setWidgetRelativeValue = function( rel, widget )
{
    widget.value = parseInt( rel * widget.maximum, 10 );
}

/*
 * Returns a sliders relative position.
 */
Configuration.prototype.widgetRelativeValue = function( widget )
{
    return parseFloat( widget.value ) / parseFloat( widget.maximum );
}

/*
 * Setup the configuration menu.
 */
Configuration.prototype.setupConfigurationMenu = function()
{
    Amarok.Window.addSettingsMenu( "iplmenu", qsTranslate( "configuration", "Intelligent Playlist" ), "amarok" );
    this.m_configurationMenu = new QMenu( qsTranslate( "configuration", "IPL Configuration" ) );
    Amarok.Window.SettingsMenu.iplmenu.setMenu( this.m_configurationMenu );

    this.m_showConfigurationDialogAction = this.m_configurationMenu.addAction( qsTranslate( "configuration", "Configure ..." ) );
    this.m_showConfigurationDialogAction.objectName = "m_showConfigurationDialogAction";
    this.m_showConfigurationDialogAction[ 'triggered()' ].connect( this, this.showConfiguration );

    this.m_enableIntelligentPlaylistAction = this.m_configurationMenu.addAction( qsTranslate( "configuration", "Enable Intelligent Playlist" ) );
    this.m_enableIntelligentPlaylistAction.objectName = "m_enableIntelligentPlaylistAction" ;
    this.m_enableIntelligentPlaylistAction.checkable = true;
    this.m_enableIntelligentPlaylistAction[ 'triggered(bool)' ].connect( this, this.onChangeEnabledState );


    this.m_currentTrackAction = this.m_configurationMenu.addAction( qsTranslate( "configuration", "Current Track" ) );
    this.m_currentTrackMenu = new QMenu( qsTranslate( "configuration", "Current Track" ) );
    this.m_currentTrackAction.setMenu( this.m_currentTrackMenu );

    this.m_approveCurrentTrackAction = this.m_currentTrackMenu.addAction( qsTranslate( "configuration", "Approve Selection" ) );
    this.m_approveCurrentTrackAction.objectName = "m_approveCurrentTrackAction";
    this.m_approveCurrentTrackAction[ 'triggered()' ].connect( this.m_plugin.playlist, this.m_plugin.playlist.approveCurrentTrack );

    this.m_disapproveCurrentTrackAction = this.m_currentTrackMenu.addAction( qsTranslate( "configuration", "Disapprove Selection" ) );
    this.m_disapproveCurrentTrackAction.objectName = "m_disapproveCurrentTrackAction";
    this.m_disapproveCurrentTrackAction[ 'triggered()' ].connect( this.m_plugin.playlist, this.m_plugin.playlist.disapproveCurrentTrack );
    
    this.m_setSimilarArtistsAction = this.m_currentTrackMenu.addAction( qsTranslate( "configuration", "Select Similar Artists" ) );
    this.m_setSimilarArtistsAction.objectName = "m_setSimilarArtistsAction" ;
    this.m_setSimilarArtistsAction[ 'triggered()' ].connect( this, this.editSimilarArtistsAndSave );

    this.m_setSimilarGenresAction = this.m_currentTrackMenu.addAction( qsTranslate( "configuration", "Select Similar Genres" ) );
    this.m_setSimilarGenresAction.objectName = "m_setSimilarGenresAction";
    this.m_setSimilarGenresAction[ 'triggered()' ].connect( this, this.editSimilarGenresAndSave );
}

/*
 * Setup the tools menu.
 */
Configuration.prototype.setupToolsMenu = function()
{
    Amarok.Window.addToolsMenu( "ipltoolmenu", qsTranslate( "configuration", "Intelligent Playlist" ), "amarok" );
    this.m_toolsMenu = new QMenu( "IPL Tools" );
    Amarok.Window.ToolsMenu.ipltoolmenu.setMenu( this.m_toolsMenu );

    this.m_showTrackCacheAction = this.m_toolsMenu.addAction( qsTranslate( "configuration", "Track Cache" ) );
    this.m_showTrackCacheAction.objectName = "m_showTrackCacheAction";
    this.m_showTrackCacheAction[ 'triggered()' ].connect( this.m_plugin.cache, this.m_plugin.cache.showContents );

    this.m_moodbarContentsAction = this.m_toolsMenu.addAction( qsTranslate( "configuration", "Moodbar Cache" ) );
    this.m_moodbarContentsAction.objectName = "m_moodbarContentsAction";
    this.m_moodbarContentsAction[ 'triggered()' ].connect( this.m_plugin.moodbarManager, this.m_plugin.moodbarManager.show );

    this.m_similarApplet = this.m_toolsMenu.addAction( qsTranslate( "configuration", "Similar Artists && Genres" ) );
    this.m_similarApplet.objectName = "m_similarApplet";
    this.m_similarApplet[ 'triggered()' ].connect( this.m_plugin.applet, this.m_plugin.applet.show );

    this.m_similarTracksAnalyzer = this.m_toolsMenu.addAction( qsTranslate( "configuration", "Analyze Similar Tracks" ) );
    this.m_similarTracksAnalyzer.objectName = "m_similarTracksAnalyzer";
    this.m_similarTracksAnalyzer[ 'triggered()' ].connect( this.m_plugin.similarTracksAnalyzer, this.m_plugin.similarTracksAnalyzer.show );
}

/*
 * Setup some actions in GUI.
 */
Configuration.prototype.setupActions = function()
{
    this.m_configForm.buttonBox['accepted()'].connect( this, this.onAccept );
    this.m_configForm.buttonBox['rejected()'].connect( this, this.onReject );
    this.m_stringModeTab.toStrictListButton['clicked()'].connect( this, this.onMoveToStrict );
    this.m_stringModeTab.toFuzzyListButton['clicked()'].connect( this, this.onMoveToFuzzy );
    this.m_similarTab.editSimilarArtistsButton['clicked()'].connect( this, this.editSimilarArtists );
    this.m_similarTab.editSimilarGenresButton['clicked()'].connect( this, this.editSimilarGenres );
}

/*
 * Update the menu item entry indicating, whether the plugin is activated.
 */
Configuration.prototype.updateGui = function()
{
    this.m_enableIntelligentPlaylistAction.checked = this.m_enabled;
}

/*
 * Returns weight, if 0.0 <= weight <= 1.0.
 * Returns 0.0, if weight < 0. Returns 1.0 if weight > 1.0.
 */
Configuration.prototype.fitWeight = function( weight )
{
    return Math.min( 1.0, Math.max( 0.0, weight ) );
}