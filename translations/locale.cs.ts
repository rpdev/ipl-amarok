<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs_CZ">
<context>
    <name>Form</name>
    <message>
        <location filename="../config.ui" line="17"/>
        <source>Intelligent Playlist</source>
        <translation>Chytrý seznam skladeb</translation>
    </message>
    <message>
        <location filename="../config.ui" line="27"/>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <location filename="../config.ui" line="33"/>
        <source>Check this to let the plugin generate playlist on-the-fly.</source>
        <translation>Zaškrtněte tuto volbu, aby přídavný modul vytvořil seznam skladeb za letu.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="36"/>
        <source>Enable the Intelligent Playlist Mode</source>
        <translation>Povolit režim chytrého seznamu skladeb</translation>
    </message>
    <message>
        <location filename="../config.ui" line="43"/>
        <source>Accept Songs that are more similar than:</source>
        <translation>Přijmout písničky, které jsou podobnější než:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="53"/>
        <source>Threshold value for selecting tracks.
While searching for similar tracks, a track gets selected if its calculated similarity value is greater than this value.</source>
        <translation>Prahová hodnota pro vybrané skladby.
Při hledání podobných skladeb je skladba vybrána, jestliže je její vypočítaná hodnota podobnosti větší než tato hodnota.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="79"/>
        <source>Distinct</source>
        <translation>Odlišné</translation>
    </message>
    <message>
        <location filename="../config.ui" line="86"/>
        <source>Identical</source>
        <translation>Shodné</translation>
    </message>
    <message>
        <location filename="../config.ui" line="96"/>
        <source>Maximum tracks to investigate:</source>
        <translation>Největší počet skladeb, který se vyšetří:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="122"/>
        <source>On every track change, the plugin will select this number of random tracks to find a similar one.</source>
        <translation>Při každé změně skladby přídavný modul zvolí tento počet náhodných skladeb, aby našel jednu podobnou.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="147"/>
        <source>Upcoming tracks:</source>
        <translation>Nadcházející skladby:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="154"/>
        <source>Played tracks:</source>
        <translation>Přehrávané skladby:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="161"/>
        <source>The number of upcoming tracks to show in the playlist.</source>
        <translation>Počet nadcházejících skladeb, které se ukážou v seznamu skladeb.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="177"/>
        <source>The number of already played tracks to keep in the playlist.</source>
        <translation>Počet již přehrávaných skladeb, které zůstanou v seznamu skladeb.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="193"/>
        <source>Checking this will interpret some user input (such as volume changes) as feedback to automatically adjust weights.</source>
        <translation>Při zaškrtnutí této volby budou některé uživatelovy vstupy (jako jsou změny hlasitosti) vykládány jako zpětná vazba, aby se automaticky upravil význam.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="196"/>
        <source>Enable Gestures as User Feedback</source>
        <translation>Povolit gesta jako uživatelovu zpětnou vazbu</translation>
    </message>
    <message>
        <location filename="../config.ui" line="224"/>
        <source>Cache Size:</source>
        <translation>Velikost vyrovnávací paměti:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="231"/>
        <source>The number of tracks to keep in the internal cache.</source>
        <translation>Počet skladeb, které mají zůstat ve vnitřní vyrovnávací paměti.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="234"/>
        <source> Tracks</source>
        <translation> Skladby</translation>
    </message>
    <message>
        <location filename="../config.ui" line="256"/>
        <source>Match...</source>
        <translation>Shoda...</translation>
    </message>
    <message>
        <location filename="../config.ui" line="263"/>
        <source>Calculate the similarity of a track against each track in the playlist and use the highest similarity value.</source>
        <translation>Spočítat podobnost skladby v porovnání s každou skladbou v seznamu skladeb a použít nejvyšší hodnotu podobnosti.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="266"/>
        <source>at least one track</source>
        <translation>alespoň jedna skladba</translation>
    </message>
    <message>
        <location filename="../config.ui" line="273"/>
        <source>Calculate similarity of a track against the current playlist.</source>
        <translation>Spočítat podobnost skladby v porovnání s nynějším seznamem skladeb.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="276"/>
        <source>complete Playlist</source>
        <translation>celý seznam skladeb</translation>
    </message>
    <message>
        <location filename="../config.ui" line="284"/>
        <source>Attributes</source>
        <translation>Vlastnosti</translation>
    </message>
    <message>
        <location filename="../config.ui" line="290"/>
        <source>Include the track names in the search.</source>
        <translation>Zahrnout názvy skladeb do hledání.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="293"/>
        <source>Track Names</source>
        <translation>Názvy skladeb</translation>
    </message>
    <message>
        <location filename="../config.ui" line="303"/>
        <source>Include years in the search.</source>
        <translation>Zahrnout roky do hledání.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="306"/>
        <location filename="../config.ui" line="460"/>
        <location filename="../config.ui" line="967"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location filename="../config.ui" line="316"/>
        <source>Include album names in the similarity search.</source>
        <translation>Zahrnout názvy alb do hledání podobnosti.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="319"/>
        <location filename="../config.ui" line="840"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../config.ui" line="329"/>
        <source>Include scores in the search.</source>
        <translation>Zahrnout výsledky do hledání.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="332"/>
        <location filename="../config.ui" line="483"/>
        <location filename="../config.ui" line="994"/>
        <source>Score</source>
        <translation>Výsledek</translation>
    </message>
    <message>
        <location filename="../config.ui" line="342"/>
        <source>Include artist names in the search.</source>
        <translation>Zahrnout jména umělců do hledání.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="345"/>
        <location filename="../config.ui" line="707"/>
        <location filename="../config.ui" line="813"/>
        <source>Artist</source>
        <translation>Umělec</translation>
    </message>
    <message>
        <location filename="../config.ui" line="355"/>
        <source>Include the rating in the search.</source>
        <translation>Zahrnout hodnocení do hledání.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="358"/>
        <location filename="../config.ui" line="506"/>
        <location filename="../config.ui" line="1001"/>
        <source>Rating</source>
        <translation>Hodnocení</translation>
    </message>
    <message>
        <location filename="../config.ui" line="368"/>
        <source>Include composer names in the search.</source>
        <translation>Zahrnout jména skladatelů do hledání.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="371"/>
        <location filename="../config.ui" line="867"/>
        <source>Composer</source>
        <translation>Skladatel</translation>
    </message>
    <message>
        <location filename="../config.ui" line="381"/>
        <location filename="../config.ui" line="407"/>
        <source>Include moodbars in the search.</source>
        <translation>Zahrnout náladové pruhy do hledání.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="384"/>
        <location filename="../config.ui" line="746"/>
        <location filename="../config.ui" line="1008"/>
        <source>Genre</source>
        <translation>Žánr</translation>
    </message>
    <message>
        <location filename="../config.ui" line="410"/>
        <source>Moodbars</source>
        <translation>Náladové pruhy</translation>
    </message>
    <message>
        <location filename="../config.ui" line="427"/>
        <source>If a random track is found, which bears the same label as a track in the playlist, immediately add this track to the playlist (circumventing the similarity calculation).</source>
        <translation>Pokud je nalezena náhodná skladba, která nese tentýž štítek, jaký má skladba v seznamu skladeb, přidat tuto skladbu okamžitě do seznamu skladeb (obcházeje výpočet podobnosti).</translation>
    </message>
    <message>
        <location filename="../config.ui" line="430"/>
        <source>Prefer Files in same Group</source>
        <translation>Upřednostňovat soubory ve stejné skupině</translation>
    </message>
    <message>
        <location filename="../config.ui" line="437"/>
        <source>If a random track is found, such that one of the attributes is marked as explicitly similar to the appropriate attribute of a track in the playlist, immediately add this track to the playlist.</source>
        <translation>Pokud je nalezena náhodná skladba, taková, u které je jedna z vlastností označena jako jasně podobná s náležitou vlastností skladby v seznamu skladeb, přidat tuto skladbu okamžitě do seznamu skladeb.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="440"/>
        <source>Prefer Explicit Similar Tracks</source>
        <translation>Upřednostňovat jasně podobné skladby</translation>
    </message>
    <message>
        <location filename="../config.ui" line="448"/>
        <source>Numbers</source>
        <translation>Čísla</translation>
    </message>
    <message>
        <location filename="../config.ui" line="467"/>
        <source>Maximum difference between years up to which to assume some similarity.</source>
        <translation>Největší rozdíl mezi roky, až do kterého předpokládat nějakou podobnost.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="490"/>
        <source>Maximum difference between scores up to which to assume some similarity.</source>
        <translation>Největší rozdíl mezi výsledky, až do kterého předpokládat nějakou podobnost.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="513"/>
        <source>Maximum difference between ratings up to which to assume some similarity.</source>
        <translation>Největší rozdíl mezi hodnoceními, až do kterého předpokládat nějakou podobnost.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="540"/>
        <source>Strings</source>
        <translation>Řetězce</translation>
    </message>
    <message>
        <location filename="../config.ui" line="546"/>
        <source>Strict Matching:</source>
        <translation>Přesná shoda:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="553"/>
        <source>Use strict matching for these attributes. In strict matching, two strings are either equal or different.</source>
        <translation>Použít přesnou shodu pro tyto vlastnosti. Při přesné shodě jsou dva řetězce buď stejné nebo rozdílné.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="560"/>
        <source>Use fuzzy matching for these attributes. In fuzzy matching, a floating similarity value is assigned to the strings ranging from &quot;completely different&quot; up to &quot;identical&quot;.</source>
        <translation>Použít nepřesnou shodu pro tyto vlastnosti. Při nepřesné shodě je řetězcům přiřazena volná hodnota podobnosti pohybující se od &quot;úplně rozdílného&quot; po &quot;stejné&quot;.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="567"/>
        <source>Fuzzy Marching:</source>
        <translation>Nepřesná shoda:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="600"/>
        <source>Move selected attribute to the left.</source>
        <translation>Přesunout vybranou vlastnost nalevo.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="603"/>
        <source>&lt;&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../config.ui" line="610"/>
        <source>Move selected attribute to the right.</source>
        <translation>Přesunout vybranou vlastnost napravo.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="613"/>
        <source>&gt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../config.ui" line="620"/>
        <source>Fuzzy String Comparison</source>
        <translation>Nepřesné srovnání řetězců</translation>
    </message>
    <message>
        <location filename="../config.ui" line="626"/>
        <source>Fuzzy Algorithm:</source>
        <translation>Nepřesný algoritmus:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="633"/>
        <source>Use Levenshtein Distance as fuzzy string matching algorithm. Recommended for weakly tagged collections.</source>
        <translation>Použít Levenshteinův rozdíl jako algoritmus pro porovnávání nepřesných řetězců. Doporučeno pro sbírky značkované každý týden.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="636"/>
        <source>Levenshtein Distance</source>
        <translation>Levenshteinův rozdíl</translation>
    </message>
    <message>
        <location filename="../config.ui" line="643"/>
        <source>Blacklist:</source>
        <translation>Černá listina:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="650"/>
        <source>Words to ignore when using the Shared Words algorithm. Separate by whitespace, e.g.:
The the a an</source>
        <translation>Slova, která se mají přehlížet, když se používá algoritmus Sdílená slova. Oddělujte mezerami, např.:
The the a an</translation>
    </message>
    <message>
        <location filename="../config.ui" line="658"/>
        <source>Use Shared Words as fuzzy string matching algorithm. Recommended for well tagged collections.</source>
        <translation>Použít Sdílená slova jako algoritmus pro porovnávání nepřesných řetězců. Doporučeno pro dobře značkované sbírky.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="661"/>
        <source>Shared Words</source>
        <translation>Sdílená slova</translation>
    </message>
    <message>
        <location filename="../config.ui" line="672"/>
        <source>Explicit Similarities</source>
        <translation>Jasné podobnosti</translation>
    </message>
    <message>
        <location filename="../config.ui" line="678"/>
        <location filename="../config.ui" line="712"/>
        <location filename="../config.ui" line="1041"/>
        <source>Similar Artists</source>
        <translation>Podobní umělci</translation>
    </message>
    <message>
        <location filename="../config.ui" line="685"/>
        <source>This table holds for each artist a list of other artists, which you think are similar. This can greatly improve the similarity calculation.</source>
        <translation>Tato tabulka přechovává pro každého umělce seznam jiných umělců, u nichž si myslíte, že jsou podobní. Tento prostředek může skvěle vylepšit výpočet podobnosti.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="720"/>
        <location filename="../config.ui" line="751"/>
        <location filename="../config.ui" line="1048"/>
        <source>Similar Genres</source>
        <translation>Podobné žánry</translation>
    </message>
    <message>
        <location filename="../config.ui" line="727"/>
        <source>This table holds for each genre a list of other genres, which you think are similar. This can greatly improve the similarity calculation.</source>
        <translation>Tato tabulka přechovává pro každý žánr seznam jiných žánrů, u nichž si myslíte, že jsou podobné. Tento prostředek může skvěle vylepšit výpočet podobnosti.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="759"/>
        <source>Edit similar artists for the currently selected artist.</source>
        <translation>Upravit podobné umělce pro nyní vybraného umělce.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="762"/>
        <source>Edit Similar Artists</source>
        <translation>Upravit podobné umělce</translation>
    </message>
    <message>
        <location filename="../config.ui" line="769"/>
        <source>Edit similar genres for the currently selected genre.</source>
        <translation>Upravit podobné žánry pro nyní vybraný žánr.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="772"/>
        <source>Edit Similar Genres</source>
        <translation>Upravit podobné žánry</translation>
    </message>
    <message>
        <location filename="../config.ui" line="780"/>
        <source>Weights</source>
        <translation>Význam</translation>
    </message>
    <message>
        <location filename="../config.ui" line="786"/>
        <source>Track Title</source>
        <translation>Název skladby</translation>
    </message>
    <message>
        <location filename="../config.ui" line="793"/>
        <location filename="../config.ui" line="820"/>
        <location filename="../config.ui" line="847"/>
        <location filename="../config.ui" line="874"/>
        <location filename="../config.ui" line="894"/>
        <location filename="../config.ui" line="914"/>
        <location filename="../config.ui" line="934"/>
        <location filename="../config.ui" line="974"/>
        <location filename="../config.ui" line="1055"/>
        <location filename="../config.ui" line="1075"/>
        <location filename="../config.ui" line="1095"/>
        <source>Move to the left to reduce the importance of the attribute in the calculation. Move to the right to increase the importance.
Note that you are adviced to not directly modify these settings. Rather use the tracks analyzer or provide the plugin with feedback to automatically adjust these values.</source>
        <translation>Přesunout nalevo kvůli omezení důležitosti vlastnosti při výpočtu. Přesunout napravo kvůli zvýšení důležitosti.
Povšimněte si, že se vám neradí, abyste přímo měnil tato nastavení. raději použijte analyzátor skladeb nebo poskytněte přídavný modul se zpětnou vazbou pro automatické přizpůsobení těchto hodnot.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="1031"/>
        <source>Don&apos;t touch this!</source>
        <translation>Nedotýkat se!</translation>
    </message>
    <message>
        <location filename="../config.ui" line="1115"/>
        <source>Moodbar</source>
        <translation>Náladový pruh</translation>
    </message>
</context>
<context>
    <name>SelectSimilarityForm</name>
    <message>
        <location filename="../selectsimilarityform.ui" line="14"/>
        <source>Select Similar Tracks</source>
        <translation>Vybrat podobné skladby</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="20"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Are these both tracks similar?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Jsou obě tyto skladby podobné?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="34"/>
        <source>Track 1:</source>
        <translation>Skladba 1:</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="48"/>
        <source>Track 2:</source>
        <translation>Skladba 2:</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="62"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;yes&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Yes, similar!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;yes&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Ano, jsou podobné!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="73"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;no&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;No, these are totally different!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;yes&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Ne, jsou zcela odlišné!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="84"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;next&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;I don&apos;t know...&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;next&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Nevím...&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="108"/>
        <source>Start Analyzation</source>
        <translation>Začít s rozborem</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="115"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
</context>
<context>
    <name>SimilarForm</name>
    <message>
        <location filename="../similarform.ui" line="17"/>
        <source>Form</source>
        <translation>Formulář</translation>
    </message>
    <message>
        <location filename="../similarform.ui" line="23"/>
        <source>SimilarXXX for</source>
        <translation>PodobnéXXX pro</translation>
    </message>
    <message>
        <location filename="../similarform.ui" line="30"/>
        <source>The attribute to select similar attributes for.</source>
        <translation>Vlastnost, pro kterou vybrat podobné vlastnosti.</translation>
    </message>
    <message>
        <location filename="../similarform.ui" line="47"/>
        <source>Type here to filter the below list.</source>
        <translation>Pište sem, abyste přefiltrovali seznam dole.</translation>
    </message>
    <message>
        <location filename="../similarform.ui" line="54"/>
        <source>Select similar values by checking them.</source>
        <translation>Vyberte podobné hodnoty tím, že je zaškrtnete.</translation>
    </message>
</context>
<context>
    <name>SimilarPlasmoid</name>
    <message>
        <location filename="../similarplasmoid.ui" line="14"/>
        <source>Similar Artists and Genres</source>
        <translation>Podobní umělci a žánry</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="26"/>
        <source>ARTIST1 and ARTIST2?</source>
        <translation>UMĚLEC 1 a UMĚLEC 2?</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="45"/>
        <source>Discard the collected statistics for the shown artists.</source>
        <translation>Vyřadit sebranou statistiku pro zobrazené umělce.</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="48"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;removeFromSimilarArtists&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Never mind!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;removeFromSimilarArtists&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Na tom nezáleží!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="62"/>
        <source>Similar Genres</source>
        <translation>Podobné žánry</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="84"/>
        <source>GENRE1 and GENRE2?</source>
        <translation>ŽÁNR 1 a ŽÁNR 2?</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="107"/>
        <source>Similar Artists</source>
        <translation>Podobní umělci</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="123"/>
        <source>Discard collected statistics for the shown genres.</source>
        <translation>Vyřadit sebranou statistiku pro zobrazené žánry.</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="126"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;removeFromSimilarGenres&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Never mind!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;removeFromSimilarArtists&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Na tom nezáleží!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="143"/>
        <source>Mark the shown genres as similar to each other.</source>
        <translation>Označit zobrazené žánry jako vzájemně si podobné.</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="146"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;addToSimilarGenres&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Yes!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;addToSimilarGenres&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Ano!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="163"/>
        <source>Mark the shown artists as similar to each other.</source>
        <translation>Označit zobrazené umělce jako vzájemně si podobné.</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="166"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;addToSimilarArtists&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Yes!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;addToSimilarGenres&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Ano!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>configuration</name>
    <message>
        <location filename="../Configuration.js" line="511"/>
        <source>Track Name</source>
        <translation>Název skladby</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="512"/>
        <source>Artist Name</source>
        <translation>Umělcovo jméno</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="513"/>
        <source>Album Name</source>
        <translation>Název alba</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="514"/>
        <source>Composer Name</source>
        <translation>Skladatelovo jméno</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="515"/>
        <source>Genre Name</source>
        <translation>Název žánru</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="516"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="517"/>
        <source>Score</source>
        <translation>Výsledek</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="518"/>
        <source>Rating</source>
        <translation>Hodnocení</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="519"/>
        <source>Similar Artists</source>
        <translation>Podobní umělci</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="520"/>
        <source>Similar Genres</source>
        <translation>Podobné žánry</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="521"/>
        <source>Moodbar</source>
        <translation>Náladový pruh</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1137"/>
        <location filename="../Configuration.js" line="1177"/>
        <source>Intelligent Playlist</source>
        <translation>Chytrý seznam skladeb</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1138"/>
        <source>IPL Configuration</source>
        <translation>Nastavení chytrého seznamu skladeb</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1141"/>
        <source>Configure ...</source>
        <translation>Nastavit...</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1145"/>
        <source>Enable Intelligent Playlist</source>
        <translation>Povolit chytrý seznam skladeb</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1151"/>
        <location filename="../Configuration.js" line="1152"/>
        <source>Current Track</source>
        <translation>Současná skladba</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1155"/>
        <source>Approve Selection</source>
        <translation>Schválit výběr</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1159"/>
        <source>Disapprove Selection</source>
        <translation>Odmítnout výběr</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1163"/>
        <source>Select Similar Artists</source>
        <translation>Vybrat podobné umělce</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1167"/>
        <source>Select Similar Genres</source>
        <translation>Vybrat podobné žánry</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1181"/>
        <source>Track Cache</source>
        <translation>Vyrovnávací paměť skladby</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1185"/>
        <source>Moodbar Cache</source>
        <translation>Vyrovnávací paměť náladového pruhu</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1189"/>
        <source>Similar Artists &amp;&amp; Genres</source>
        <translation>Podobní umělci &amp;&amp; žánry</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1193"/>
        <source>Analyze Similar Tracks</source>
        <translation>Rozebrat podobné skladby</translation>
    </message>
</context>
<context>
    <name>moodbarmanager</name>
    <message>
        <location filename="../MoodbarManager.js" line="64"/>
        <source>URL</source>
        <translation>Adresa (URL)</translation>
    </message>
    <message>
        <location filename="../MoodbarManager.js" line="65"/>
        <source>Age</source>
        <translation>Věk</translation>
    </message>
    <message>
        <location filename="../MoodbarManager.js" line="66"/>
        <source>Continuity</source>
        <translation>Spojitost</translation>
    </message>
    <message>
        <location filename="../MoodbarManager.js" line="436"/>
        <source>Moodbar Cache - %1 / %2</source>
        <translation>Vyrovnávací paměť náladového pruhu - %1 / %2</translation>
    </message>
</context>
<context>
    <name>similarform</name>
    <message>
        <location filename="../SimilarForm.js" line="117"/>
        <source>Similar Artists for</source>
        <translation>Podobní umělci pro</translation>
    </message>
    <message>
        <location filename="../SimilarForm.js" line="121"/>
        <source>Similar Genres for</source>
        <translation>Podobné žánry pro</translation>
    </message>
</context>
<context>
    <name>similarityvector</name>
    <message>
        <location filename="../SimilarityVector.js" line="123"/>
        <source>Got positive feedback. Weights adjusted.</source>
        <translation>Obdržena kladná zpětná vazba. Význam upraven.</translation>
    </message>
    <message>
        <location filename="../SimilarityVector.js" line="126"/>
        <source>Got negative feedback. Weights adjusted.</source>
        <translation>Obdržena záporná zpětná vazba. Význam upraven.</translation>
    </message>
</context>
<context>
    <name>similartracksanalizer</name>
    <message>
        <location filename="../SimilarTracksAnalyzer.js" line="112"/>
        <source>Comparing Tracks - %p%</source>
        <translation>Srovnávají se skladby - %p%</translation>
    </message>
    <message>
        <location filename="../SimilarTracksAnalyzer.js" line="119"/>
        <source>Generating Weights - %p%</source>
        <translation>Srovnávají se významy - %p%</translation>
    </message>
    <message>
        <location filename="../SimilarTracksAnalyzer.js" line="245"/>
        <source>&lt;b&gt;Result&lt;/b&gt;&lt;br/&gt;&lt;b&gt;Minimal Similarity:&lt;/b&gt; %1&lt;br/&gt;</source>
        <translation>&lt;b&gt;Výsledek&lt;/b&gt;&lt;br/&gt;&lt;b&gt;Zanedbatelná podobnost:&lt;/b&gt; %1&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../SimilarTracksAnalyzer.js" line="251"/>
        <source>Apply these values?</source>
        <translation>Použít tyto hodnoty?</translation>
    </message>
</context>
<context>
    <name>similarwidget</name>
    <message>
        <location filename="../SimilarWidget.js" line="53"/>
        <source>&lt;i&gt;No more possible similar artists found...&lt;/i&gt;</source>
        <translation>&lt;i&gt;Více možných podobných umělců nebylo nalezeno...&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../SimilarWidget.js" line="58"/>
        <location filename="../SimilarWidget.js" line="70"/>
        <source>&lt;b&gt;%1&lt;/b&gt; and &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; a &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../SimilarWidget.js" line="65"/>
        <source>&lt;i&gt;No more possible similar genres found...&lt;/i&gt;</source>
        <translation>&lt;i&gt;Více možných podobných žánrů nebylo nalezeno...&lt;/i&gt;</translation>
    </message>
</context>
<context>
    <name>trackcache</name>
    <message>
        <location filename="../TrackCache.js" line="145"/>
        <source>Track Cache</source>
        <translation>Vyrovnávací paměť skladby</translation>
    </message>
    <message>
        <location filename="../TrackCache.js" line="148"/>
        <source>Similarity</source>
        <translation>Podobnost</translation>
    </message>
    <message>
        <location filename="../TrackCache.js" line="149"/>
        <source>TTL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../TrackCache.js" line="150"/>
        <source>Track</source>
        <translation>Skladba</translation>
    </message>
</context>
</TS>
