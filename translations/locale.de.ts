<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>Form</name>
    <message>
        <location filename="../config.ui" line="17"/>
        <source>Intelligent Playlist</source>
        <translation>Intelligente Wiedergabeliste</translation>
    </message>
    <message>
        <location filename="../config.ui" line="27"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location filename="../config.ui" line="33"/>
        <source>Check this to let the plugin generate playlist on-the-fly.</source>
        <translation>Aktivieren, um das Plugin die Wiedergabeliste dynamisch zu generieren.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="36"/>
        <source>Enable the Intelligent Playlist Mode</source>
        <translation>Aktiviert den Modus &quot;Intelligente Wiedergabeliste&quot;</translation>
    </message>
    <message>
        <location filename="../config.ui" line="43"/>
        <source>Accept Songs that are more similar than:</source>
        <translation>Akzeptiere Stücke, die mindestens diesen Ähnlichkeitswert haben:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="53"/>
        <source>Threshold value for selecting tracks.
While searching for similar tracks, a track gets selected if its calculated similarity value is greater than this value.</source>
        <translation>Schwellwert für Stücke, bevor diese ausgewählt werden.
Während das Plugin nach passenden Stücken sucht, werden Stücke sofort ausgewählt, wenn sie diesen Wert überschreiten.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="79"/>
        <source>Distinct</source>
        <translation>Verschieden</translation>
    </message>
    <message>
        <location filename="../config.ui" line="86"/>
        <source>Identical</source>
        <translation>Gleich</translation>
    </message>
    <message>
        <location filename="../config.ui" line="96"/>
        <source>Maximum tracks to investigate:</source>
        <translation>Maximale Zahl an zu untersuchenden Stücken:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="122"/>
        <source>On every track change, the plugin will select this number of random tracks to find a similar one.</source>
        <translation>Bei jedem Wechsel des aktuellen Stücks wird das Plugin die angegebene Anzahl an zufällig gewählten Stücken untersuchen um ein ähnliches Stück zu finden.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="147"/>
        <source>Upcoming tracks:</source>
        <translation>Folgende Stücke:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="154"/>
        <source>Played tracks:</source>
        <translation>Bereits gespielte Stücke:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="161"/>
        <source>The number of upcoming tracks to show in the playlist.</source>
        <translation>Die Anzahl der zukünftig gespielten Stücke, die bereits in der Wiedergabeliste sichtbar sind.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="177"/>
        <source>The number of already played tracks to keep in the playlist.</source>
        <translation>Die Anzahl an Stücken, die bereits gespielt wurden und die noch in der Wiedergabeliste verbleiben sollen.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="193"/>
        <source>Checking this will interpret some user input (such as volume changes) as feedback to automatically adjust weights.</source>
        <translation>Wird diese Option aktiviert, interpretiert das Plugin einige Eingaben des Benutzers (so wie Lautstärkeänderungen) als Rückmeldung um automatisch die Gewichtigen der Attribute anzupassen.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="196"/>
        <source>Enable Gestures as User Feedback</source>
        <translation>Aktiviere Gesten als Benutzerrückmeldung</translation>
    </message>
    <message>
        <location filename="../config.ui" line="224"/>
        <source>Cache Size:</source>
        <translation>Puffergröße:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="231"/>
        <source>The number of tracks to keep in the internal cache.</source>
        <translation>Die Anzahl an bereits analysierten Stücken, die intern im Speicher gehalten werden.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="234"/>
        <source> Tracks</source>
        <translation> Stücke</translation>
    </message>
    <message>
        <location filename="../config.ui" line="256"/>
        <source>Match...</source>
        <translation>Prüfe gegen...</translation>
    </message>
    <message>
        <location filename="../config.ui" line="263"/>
        <source>Calculate the similarity of a track against each track in the playlist and use the highest similarity value.</source>
        <translation>Berechnet die Ähnlichkeit gegen alle Stücke in der Wiedergabeliste und nutzt den höchsten berechneten Wert.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="266"/>
        <source>at least one track</source>
        <translation>mindestens ein Stück</translation>
    </message>
    <message>
        <location filename="../config.ui" line="273"/>
        <source>Calculate similarity of a track against the current playlist.</source>
        <translation>Berechne die Ähnlichkeit eines Stückes gegen die gesamte aktuelle Wiedergabeliste.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="276"/>
        <source>complete Playlist</source>
        <translation>die komplette Wiedergabeliste</translation>
    </message>
    <message>
        <location filename="../config.ui" line="284"/>
        <source>Attributes</source>
        <translation>Attribute</translation>
    </message>
    <message>
        <location filename="../config.ui" line="290"/>
        <source>Include the track names in the search.</source>
        <translation>Beziehe Namen der Stücke in die Suche ein.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="293"/>
        <source>Track Names</source>
        <translation>Stücknamen</translation>
    </message>
    <message>
        <location filename="../config.ui" line="303"/>
        <source>Include years in the search.</source>
        <translation>Beziehe das Jahr in die Suche ein.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="306"/>
        <location filename="../config.ui" line="460"/>
        <location filename="../config.ui" line="967"/>
        <source>Year</source>
        <translation>Jahr</translation>
    </message>
    <message>
        <location filename="../config.ui" line="316"/>
        <source>Include album names in the similarity search.</source>
        <translation>Beziehe den Namen der Alben in die Suche ein.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="319"/>
        <location filename="../config.ui" line="840"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../config.ui" line="329"/>
        <source>Include scores in the search.</source>
        <translation>Beziehe die Punkte in die Suche ein.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="332"/>
        <location filename="../config.ui" line="483"/>
        <location filename="../config.ui" line="994"/>
        <source>Score</source>
        <translation>Punkte</translation>
    </message>
    <message>
        <location filename="../config.ui" line="342"/>
        <source>Include artist names in the search.</source>
        <translation>Beziehe den Namen der Interpreten in die Suche ein.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="345"/>
        <location filename="../config.ui" line="707"/>
        <location filename="../config.ui" line="813"/>
        <source>Artist</source>
        <translation>Interpreten</translation>
    </message>
    <message>
        <location filename="../config.ui" line="355"/>
        <source>Include the rating in the search.</source>
        <translation>Beziehe die Bewertung in die Suche ein.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="358"/>
        <location filename="../config.ui" line="506"/>
        <location filename="../config.ui" line="1001"/>
        <source>Rating</source>
        <translation>Bewertung</translation>
    </message>
    <message>
        <location filename="../config.ui" line="368"/>
        <source>Include composer names in the search.</source>
        <translation>Beziehe den Komponisten in die Suche ein.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="371"/>
        <location filename="../config.ui" line="867"/>
        <source>Composer</source>
        <translation>Komponisten</translation>
    </message>
    <message>
        <location filename="../config.ui" line="381"/>
        <location filename="../config.ui" line="407"/>
        <source>Include moodbars in the search.</source>
        <translation>Beziehe die Stimmung in die Suche ein.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="384"/>
        <location filename="../config.ui" line="746"/>
        <location filename="../config.ui" line="1008"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../config.ui" line="410"/>
        <source>Moodbars</source>
        <translation>Stimmung</translation>
    </message>
    <message>
        <location filename="../config.ui" line="427"/>
        <source>If a random track is found, which bears the same label as a track in the playlist, immediately add this track to the playlist (circumventing the similarity calculation).</source>
        <translation>Wenn ein zufälliges Stück gefunden wird, welches eine Beschriftung hat, so dass ein Stück in der aktuellen Wiedergabeliste diese Beschriftung ebenfalls hat, füge dieses Stück sofort zur Wiedergabeliste hinzu (und umgehe die Ähnlichkeitsberechnung).</translation>
    </message>
    <message>
        <location filename="../config.ui" line="430"/>
        <source>Prefer Files in same Group</source>
        <translation>Stücke mit gleichen Beschriftungen bevorzugen</translation>
    </message>
    <message>
        <location filename="../config.ui" line="437"/>
        <source>If a random track is found, such that one of the attributes is marked as explicitly similar to the appropriate attribute of a track in the playlist, immediately add this track to the playlist.</source>
        <translation>Wenn ein zufälliges Stück gefunden wird, so dass eines seiner Attribute als explizit ähnlich zu einem Attribut eines Stückes in der aktuellen Wiedergabeliste markiert ist, füge das Stück sofort zur Wiedergabeliste hinzu.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="440"/>
        <source>Prefer Explicit Similar Tracks</source>
        <translation>Stücke mit explizit ähnlichen Attributen bevorzugen</translation>
    </message>
    <message>
        <location filename="../config.ui" line="448"/>
        <source>Numbers</source>
        <translation>Zahlen</translation>
    </message>
    <message>
        <location filename="../config.ui" line="467"/>
        <source>Maximum difference between years up to which to assume some similarity.</source>
        <translation>Maximale Differenz an Jahren, bis zu denen eine gewisse Ähnlichkeit angenommen werden soll.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="490"/>
        <source>Maximum difference between scores up to which to assume some similarity.</source>
        <translation>Maximale Differenz an Punkten, bis zu der eine gewisse Ähnlichkeit angenommen werden soll.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="513"/>
        <source>Maximum difference between ratings up to which to assume some similarity.</source>
        <translation>Maximale Differenz zwischen Bewertungen, bis zu der eine gewisse Ähnlichkeit angenommen werden soll.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="540"/>
        <source>Strings</source>
        <translation>Zeichenketten</translation>
    </message>
    <message>
        <location filename="../config.ui" line="546"/>
        <source>Strict Matching:</source>
        <translation>Strikte Vergleiche:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="553"/>
        <source>Use strict matching for these attributes. In strict matching, two strings are either equal or different.</source>
        <translation>Benutze strikte Vergleiche für die aufgelisteten Attribute. Bei strikten Vergleichen sind zwei Zeichenketten entweder gleich oder verschieden.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="560"/>
        <source>Use fuzzy matching for these attributes. In fuzzy matching, a floating similarity value is assigned to the strings ranging from &quot;completely different&quot; up to &quot;identical&quot;.</source>
        <translation>Benutze fließende Vergleiche für die gelisteten Attribute. Beim fließenden Vergleich wird einem Paar von Zeichenketten ein Ähnlichkeitswert zwischen &quot;complett unterschiedlich&quot; bis zu &quot;identisch&quot; zugewiesen.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="567"/>
        <source>Fuzzy Marching:</source>
        <translation>Fließende Vergleiche:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="600"/>
        <source>Move selected attribute to the left.</source>
        <translation>Bewege gewähltes Attribut nach Links.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="603"/>
        <source>&lt;&lt;</source>
        <translation>&lt;&lt;</translation>
    </message>
    <message>
        <location filename="../config.ui" line="610"/>
        <source>Move selected attribute to the right.</source>
        <translation>Bewege gewähltes Attribut nach Rechts.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="613"/>
        <source>&gt;&gt;</source>
        <translation>&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../config.ui" line="620"/>
        <source>Fuzzy String Comparison</source>
        <translation>Fließende Zeichenkettenvergleiche</translation>
    </message>
    <message>
        <location filename="../config.ui" line="626"/>
        <source>Fuzzy Algorithm:</source>
        <translation>Algorithmus:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="633"/>
        <source>Use Levenshtein Distance as fuzzy string matching algorithm. Recommended for weakly tagged collections.</source>
        <translation>Benutze die Levenshtein-Distanz als Algorithmus. Empfohlen für Sammlungen mit fehlerhaften oder unvollständigen Attributen.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="636"/>
        <source>Levenshtein Distance</source>
        <translation>Levenshtein-Distanz</translation>
    </message>
    <message>
        <location filename="../config.ui" line="643"/>
        <source>Blacklist:</source>
        <translation>Ignorierte Wörter:</translation>
    </message>
    <message>
        <location filename="../config.ui" line="650"/>
        <source>Words to ignore when using the Shared Words algorithm. Separate by whitespace, e.g.:
The the a an</source>
        <translation>Wörter die beim Gemeinsame-Wörter-Algorithmus ignoriert werden sollen. Einzelne Wörter mit Leerzeichen trennen, z.B.:
Der der Die die Ein ein</translation>
    </message>
    <message>
        <location filename="../config.ui" line="658"/>
        <source>Use Shared Words as fuzzy string matching algorithm. Recommended for well tagged collections.</source>
        <translation>Benutze den Gemeinsame-Wörter-Algorithmus zur Berechnung fließender Zeichenkettenvergleiche. Empfohlen für Sammlungen, bei denen die Attribute der Stücke weitgehend vollständig und korrekt sind.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="661"/>
        <source>Shared Words</source>
        <translation>Gemeinsame-Wörter</translation>
    </message>
    <message>
        <location filename="../config.ui" line="672"/>
        <source>Explicit Similarities</source>
        <translation>Explizite Ähnlichkeiten</translation>
    </message>
    <message>
        <location filename="../config.ui" line="678"/>
        <location filename="../config.ui" line="712"/>
        <location filename="../config.ui" line="1041"/>
        <source>Similar Artists</source>
        <translation>Ähnliche Interpreten</translation>
    </message>
    <message>
        <location filename="../config.ui" line="685"/>
        <source>This table holds for each artist a list of other artists, which you think are similar. This can greatly improve the similarity calculation.</source>
        <translation>Diese Tabelle enthält für jeden Interpreten eine Liste an anderen Interpreten, die ähnlich sind. Dies kann die Berechnung der Ähnlichkeiten enorm verbessern.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="720"/>
        <location filename="../config.ui" line="751"/>
        <location filename="../config.ui" line="1048"/>
        <source>Similar Genres</source>
        <translation>Ähnliche Genre</translation>
    </message>
    <message>
        <location filename="../config.ui" line="727"/>
        <source>This table holds for each genre a list of other genres, which you think are similar. This can greatly improve the similarity calculation.</source>
        <translation>Diese Tabelle enthält für jedes Genre eine Liste an anderen Genren, die ähnlich sind. Dies kann die Berechnung der Ähnlichkeiten enorm verbessern.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="759"/>
        <source>Edit similar artists for the currently selected artist.</source>
        <translation>Editiere die ähnlichen Interpreten für den aktuell gewählten Interpreten.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="762"/>
        <source>Edit Similar Artists</source>
        <translation>Editiere ähnliche Interpreten</translation>
    </message>
    <message>
        <location filename="../config.ui" line="769"/>
        <source>Edit similar genres for the currently selected genre.</source>
        <translation>Editiere ähnliche Genre für das aktuell gewählte Genre.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="772"/>
        <source>Edit Similar Genres</source>
        <translation>Editiere ähnliche Genre</translation>
    </message>
    <message>
        <location filename="../config.ui" line="780"/>
        <source>Weights</source>
        <translation>Gewichtungen</translation>
    </message>
    <message>
        <location filename="../config.ui" line="786"/>
        <source>Track Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../config.ui" line="793"/>
        <location filename="../config.ui" line="820"/>
        <location filename="../config.ui" line="847"/>
        <location filename="../config.ui" line="874"/>
        <location filename="../config.ui" line="894"/>
        <location filename="../config.ui" line="914"/>
        <location filename="../config.ui" line="934"/>
        <location filename="../config.ui" line="974"/>
        <location filename="../config.ui" line="1055"/>
        <location filename="../config.ui" line="1075"/>
        <location filename="../config.ui" line="1095"/>
        <source>Move to the left to reduce the importance of the attribute in the calculation. Move to the right to increase the importance.
Note that you are adviced to not directly modify these settings. Rather use the tracks analyzer or provide the plugin with feedback to automatically adjust these values.</source>
        <translation>Nach Links bewegen, um die Wichtigkeit des Attributes in der Berechnung zu verringen. Nach Rechts bewegen, um die Wichtigkeit zu erhöhen.
Diese Werte sollten nicht per Hand verändert werden. Es wird eher empfohlen, die Stückanalyse zu verwenden oder dem Plugin Rückmeldung zu geben, um die Gewichtungen automatisch anzupassen.</translation>
    </message>
    <message>
        <location filename="../config.ui" line="1031"/>
        <source>Don&apos;t touch this!</source>
        <translation>Nicht per Hand editieren!</translation>
    </message>
    <message>
        <location filename="../config.ui" line="1115"/>
        <source>Moodbar</source>
        <translation>Stimmung</translation>
    </message>
</context>
<context>
    <name>SelectSimilarityForm</name>
    <message>
        <location filename="../selectsimilarityform.ui" line="14"/>
        <source>Select Similar Tracks</source>
        <translation>Wähle ähnliche Stücke</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="20"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Are these both tracks similar?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Sind diese Stücke ähnlich?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="34"/>
        <source>Track 1:</source>
        <translation>Stück 1:</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="48"/>
        <source>Track 2:</source>
        <translation>Stück 2:</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="62"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;yes&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Yes, similar!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;yes&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Ja, sie sind ähnlich!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="73"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;no&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;No, these are totally different!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;no&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Nein, die sind komplett verschieden!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="84"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;next&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;I don&apos;t know...&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;next&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Ich weiß es nicht...&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="108"/>
        <source>Start Analyzation</source>
        <translation>Beginne mit der Analyse</translation>
    </message>
    <message>
        <location filename="../selectsimilarityform.ui" line="115"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>SimilarForm</name>
    <message>
        <location filename="../similarform.ui" line="17"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../similarform.ui" line="23"/>
        <source>SimilarXXX for</source>
        <translation>SimilarXXX for</translation>
    </message>
    <message>
        <location filename="../similarform.ui" line="30"/>
        <source>The attribute to select similar attributes for.</source>
        <translation>Der Wert, für den Ähnliche Werte gewählt werden sollen.</translation>
    </message>
    <message>
        <location filename="../similarform.ui" line="47"/>
        <source>Type here to filter the below list.</source>
        <translation>Hier schreiben um die Liste unten zu filtern.</translation>
    </message>
    <message>
        <location filename="../similarform.ui" line="54"/>
        <source>Select similar values by checking them.</source>
        <translation>Ähnliche Werte durch aktivieren des Eintrages wählen.</translation>
    </message>
</context>
<context>
    <name>SimilarPlasmoid</name>
    <message>
        <location filename="../similarplasmoid.ui" line="14"/>
        <source>Similar Artists and Genres</source>
        <translation>Ähnliche Interpreten und Genre</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="26"/>
        <source>ARTIST1 and ARTIST2?</source>
        <translation>ARTIST1 and ARTIST2?</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="45"/>
        <source>Discard the collected statistics for the shown artists.</source>
        <translation>Verwerfe die Statistiken für die gezeigten Interpreten.</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="48"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;removeFromSimilarArtists&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Never mind!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;removeFromSimilarArtists&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Vergiss es!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="62"/>
        <source>Similar Genres</source>
        <translation>Ähnliche Genre</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="84"/>
        <source>GENRE1 and GENRE2?</source>
        <translation>GENRE1 and GENRE2?</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="107"/>
        <source>Similar Artists</source>
        <translation>Ähnliche Interpreten</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="123"/>
        <source>Discard collected statistics for the shown genres.</source>
        <translation>Verwerfe die gesammelten Statistiken für die gezeigten Genre.</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="126"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;removeFromSimilarGenres&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Never mind!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;removeFromSimilarGenres&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Vergiss es!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="143"/>
        <source>Mark the shown genres as similar to each other.</source>
        <translation>Markiere die gezeigten Genre als ähnlich.</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="146"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;addToSimilarGenres&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Yes!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;addToSimilarGenres&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Ja!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="163"/>
        <source>Mark the shown artists as similar to each other.</source>
        <translation>Markiere die gezeigten Interpreten als ähnlich.</translation>
    </message>
    <message>
        <location filename="../similarplasmoid.ui" line="166"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;addToSimilarArtists&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Yes!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Liberation Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;addToSimilarArtists&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Ja!&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>configuration</name>
    <message>
        <location filename="../Configuration.js" line="511"/>
        <source>Track Name</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="512"/>
        <source>Artist Name</source>
        <translation>Interpret</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="513"/>
        <source>Album Name</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="514"/>
        <source>Composer Name</source>
        <translation>Komponist</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="515"/>
        <source>Genre Name</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="516"/>
        <source>Year</source>
        <translation>Jahr</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="517"/>
        <source>Score</source>
        <translation>Punkte</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="518"/>
        <source>Rating</source>
        <translation>Bewertung</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="519"/>
        <source>Similar Artists</source>
        <translation>Ähnliche Interpreten</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="520"/>
        <source>Similar Genres</source>
        <translation>Ähnliche Genre</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="521"/>
        <source>Moodbar</source>
        <translation>Stimmung</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1137"/>
        <location filename="../Configuration.js" line="1177"/>
        <source>Intelligent Playlist</source>
        <translation>Intelligente Wiedergabeliste</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1138"/>
        <source>IPL Configuration</source>
        <translation>IWL Konfiguration</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1141"/>
        <source>Configure ...</source>
        <translation>Konfigurieren ...</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1145"/>
        <source>Enable Intelligent Playlist</source>
        <translation>Aktiviere die Intelligente Wiedergabeliste</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1151"/>
        <location filename="../Configuration.js" line="1152"/>
        <source>Current Track</source>
        <translation>Aktuelles Stück</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1155"/>
        <source>Approve Selection</source>
        <translation>Auswahl zustimmen</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1159"/>
        <source>Disapprove Selection</source>
        <translation>Auswahl ablehnen</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1163"/>
        <source>Select Similar Artists</source>
        <translation>Ähnliche Interpreten wählen</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1167"/>
        <source>Select Similar Genres</source>
        <translation>Ähnliche Genre wählen</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1181"/>
        <source>Track Cache</source>
        <translation>Stückpuffer</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1185"/>
        <source>Moodbar Cache</source>
        <translation>Stimmungspuffer</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1189"/>
        <source>Similar Artists &amp;&amp; Genres</source>
        <translation>Ähnliche Interpreten &amp;&amp; Genre</translation>
    </message>
    <message>
        <location filename="../Configuration.js" line="1193"/>
        <source>Analyze Similar Tracks</source>
        <translation>Stückanalyse</translation>
    </message>
</context>
<context>
    <name>moodbarmanager</name>
    <message>
        <location filename="../MoodbarManager.js" line="64"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../MoodbarManager.js" line="65"/>
        <source>Age</source>
        <translation>Alter</translation>
    </message>
    <message>
        <location filename="../MoodbarManager.js" line="66"/>
        <source>Continuity</source>
        <translation>Kontinuität</translation>
    </message>
    <message>
        <location filename="../MoodbarManager.js" line="436"/>
        <source>Moodbar Cache - %1 / %2</source>
        <translation>Moodbackspeicher - %1 / %2</translation>
    </message>
</context>
<context>
    <name>similarform</name>
    <message>
        <location filename="../SimilarForm.js" line="117"/>
        <source>Similar Artists for</source>
        <translation>Ähnliche Interpreten für</translation>
    </message>
    <message>
        <location filename="../SimilarForm.js" line="121"/>
        <source>Similar Genres for</source>
        <translation>Ähnliche Genre für</translation>
    </message>
</context>
<context>
    <name>similarityvector</name>
    <message>
        <location filename="../SimilarityVector.js" line="123"/>
        <source>Got positive feedback. Weights adjusted.</source>
        <translation>Positive Rückmeldung erhalten. Gewichtungen wurden angepasst.</translation>
    </message>
    <message>
        <location filename="../SimilarityVector.js" line="126"/>
        <source>Got negative feedback. Weights adjusted.</source>
        <translation>Negative Rückmeldung erhalten. Gewichtungen wurden angepasst.</translation>
    </message>
</context>
<context>
    <name>similartracksanalizer</name>
    <message>
        <location filename="../SimilarTracksAnalyzer.js" line="112"/>
        <source>Comparing Tracks - %p%</source>
        <translation>Vergleiche Stücke - %p%</translation>
    </message>
    <message>
        <location filename="../SimilarTracksAnalyzer.js" line="119"/>
        <source>Generating Weights - %p%</source>
        <translation>Generiere Gewichtungen - %p%</translation>
    </message>
    <message>
        <location filename="../SimilarTracksAnalyzer.js" line="245"/>
        <source>&lt;b&gt;Result&lt;/b&gt;&lt;br/&gt;&lt;b&gt;Minimal Similarity:&lt;/b&gt; %1&lt;br/&gt;</source>
        <translation>&lt;b&gt;Ergebnis&lt;/b&gt;&lt;br/&gt;&lt;b&gt;Minimale Ähnlichkeit:&lt;/b&gt; %1&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../SimilarTracksAnalyzer.js" line="251"/>
        <source>Apply these values?</source>
        <translation>Diese Werte übernehmen?</translation>
    </message>
</context>
<context>
    <name>similarwidget</name>
    <message>
        <location filename="../SimilarWidget.js" line="53"/>
        <source>&lt;i&gt;No more possible similar artists found...&lt;/i&gt;</source>
        <translation>&lt;i&gt;Keine weiteren ähnlichen Interpreten gefunden...&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../SimilarWidget.js" line="58"/>
        <location filename="../SimilarWidget.js" line="70"/>
        <source>&lt;b&gt;%1&lt;/b&gt; and &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; und &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../SimilarWidget.js" line="65"/>
        <source>&lt;i&gt;No more possible similar genres found...&lt;/i&gt;</source>
        <translation>&lt;i&gt;Keine weiteren ähnlichen Genre gefunden...&lt;/i&gt;</translation>
    </message>
</context>
<context>
    <name>trackcache</name>
    <message>
        <location filename="../TrackCache.js" line="145"/>
        <source>Track Cache</source>
        <translation>Stückpuffer</translation>
    </message>
    <message>
        <location filename="../TrackCache.js" line="148"/>
        <source>Similarity</source>
        <translation>Ähnlichkeit</translation>
    </message>
    <message>
        <location filename="../TrackCache.js" line="149"/>
        <source>TTL</source>
        <translation>TTL</translation>
    </message>
    <message>
        <location filename="../TrackCache.js" line="150"/>
        <source>Track</source>
        <translation>Stück</translation>
    </message>
</context>
</TS>
