SOURCES         =   ../Collection.js \
                    ../Configuration.js \
                    ../ExplicitSimilarityList.js \
                    ../ExplicitSimilarityListEntry.js \
                    ../IntelligentPlaylist.js \
                    ../main.js \
                    ../MoodbarManager.js \
                    ../Playlist.js \
                    ../PlaylistSolver.js \
                    ../SimilarForm.js \
                    ../SimilarityVector.js \
                    ../SimilarTracksAnalyzer.js \
                    ../SimilarWidget.js \
                    ../StringComparer.js \
                    ../Track.js \
                    ../TrackCache.js \
                    ../TrackCacheEntry.js \
                    ../Translation.js
                    
FORMS           =   ../config.ui \
                    ../selectsimilarityform.ui \
                    ../similarform.ui \
                    ../similarplasmoid.ui
                    
TRANSLATIONS    =   locale.cs.ts \
                    locale.de.ts \
                    locale.ts
