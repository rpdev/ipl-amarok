[Desktop Entry]
Icon=get-hot-new-stuff-amarok
Type=script
ServiceTypes=KPluginInfo

Name=Intelligent Playlist
Name[cs]=Chytrý seznam skladeb
Name[de]=Intelligente Wiedergabeliste

Comment=Creates dynamic playlist with similar tracks based on the current playlist.
Comment[cs]=Vytváří dynamický seznam skladeb s podobnými skladbami založený na současném seznamu skladeb.
Comment[de]=Erstellt eine dynamische Wiedergabeliste auf Grundlage der aktuellen Liste.

X-KDE-PluginInfo-Author=Martin Höher
X-KDE-PluginInfo-Email=martin@rpdev.net
X-KDE-PluginInfo-Name=IntelligentPlaylist
X-KDE-PluginInfo-Version=0.2.0
X-KDE-PluginInfo-Category=Generic
X-KDE-PluginInfo-Depends=Amarok2.0
X-KDE-PluginInfo-EnabledByDefault=false
X-KDE-PluginInfo-License=GPL
