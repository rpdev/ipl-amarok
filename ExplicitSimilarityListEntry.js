/*
 *    Copyright (C) 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Constructs a new ExplicitSimilarityListEntry item.
 * This is an entry in the list of explicitly set similar values
 * for e.g. artists or genres.
 * This class exists for encapsulating the needed data and
 * also provides some helper methods.
 */
function ExplicitSimilarityListEntry( value, similarity )
{
    this.m_value = value;
    this.m_similarity = similarity;
}

/**
 * Returns the associated value of this entry.
 */
ExplicitSimilarityListEntry.prototype.entry = function()
{
    return this.m_value;
}

/**
 * Returns the similarity value assigned to this entry.
 */
ExplicitSimilarityListEntry.prototype.similarity = function()
{
    return this.m_similarity;
}

/**
 * Returns a list of similar values.
 * The list will only return values that are not yet in the list of similar
 * values.
 * list is a ExplicitSimilarityList object.
 * decrease influenced the similarity assigned to the result values, i.e.
 * their similarity is set to this.similarity - decrease.
 */
ExplicitSimilarityListEntry.prototype.similarEntries = function( list, decrease )
{
    if ( this.m_similarity - decrease <= 0.0 )
    {
        return new Array();
    }
    var sl = new Array();
    var sims = list.similarValues( this.m_value );
    for ( var i = 0; i < sims.length; i++ )
    {
        sl.push( sims[ i ] );
    }
    var result = new Array();
    for ( var i = 0; i < sl.length; i++ )
    {
        if ( !( list.isInList( sl[ i ] ) ) )
        {
            result.push( new ExplicitSimilarityListEntry( sl[ i ], this.m_similarity - decrease ) );
        }
    }
    return result;
}