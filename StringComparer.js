/*
 *    Copyright (C) 2010 by Martin Höher <martin@rpdev.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// PUBLIC

/*
 * Creates a new StringComparer.
 *
 * String comparers are used to compare strings on a per word basis.
 * This is - in the context of track comparison - often more accurate
 * than an edit distance (e.g. Levensthein).
 * Basically, the StringComparer implements an per-word distinct Hamming
 * distance:
 * First, two input strings are split up into list containing only distinct
 * words.
 * Than, two lists are build: One holding all ( distinct ) words from both
 * input lists and one holding only the words, that are part of both strings.
 * The result of the comparison is then the length of the list with shared
 * words divided by the length of list with all words.
 *
 * The class also uses a black-list, i.e. a list of words to
 * ignore in the comparison.
 * This is useful, as typical words ( articles as "The", "A" and so on )
 * would otherwise cause "false similarity" assumptions.
 */
function StringComparer( plugin )
{
    this.m_plugin = plugin;
    this.m_wordDelimiters = new Array( " ", "-", "/", ".", ",", "+" );
    this.m_blacklist = new Array();
}

/*
 * Compare string1 and string2.
 * The result is the relative, distinct hamming distance between the words
 * of the both strings.
 */
StringComparer.prototype.compareStrings = function( string1, string2 )
{
    this.m_blacklist = this.m_plugin.configuration.sharedWordsBlacklist();
    
    var l1 = this.stringToWordList( string1 );
    var l2 = this.stringToWordList( string2 );

    var common = new Array();
    var all = new Array();

    for ( var i = 0; i < l1.length; i++ )
    {
        all.push( l1[ i ] );
    }

    for ( var i = 0; i < l2.length; i++ )
    {
        if ( this.listContains( l1, l2[ i ] ) )
        {
            common.push( l2[ i ] );
        } else
        {
            all.push( l2[ i ] );
        }
    }

    if ( all.length == 0 )
    {
        return 0.0;
    }
    return ( parseFloat( common.length ) / parseFloat( all.length ) );
}






// PRIVATE

/*
 * Creates a list with all words in the string.
 */
StringComparer.prototype.stringToWordList = function( string )
{
    // Make sure, string is not undefined
    if ( !( string ) )
    {
        string = "";
    }
    
    var result = new Array();

    var list = new Array();
    list.push( string );

    for ( var delimIdx = 0; delimIdx < this.m_wordDelimiters.length; delimIdx++ )
    {
        var delim = this.m_wordDelimiters[ delimIdx ];
        var tl = new Array();
        for ( var si = 0; si < list.length; si++ )
        {
            var temp = list[ si ].split( delim );
            for ( var tempEntryIdx = 0; tempEntryIdx < temp.length; tempEntryIdx++ )
            {
                tl.push( temp[ tempEntryIdx ] );
            }
        }
        list = tl;
    }

    for ( var i = 0; i < list.length; i++ )
    {
        if ( !( this.listContains( result, list[ i ] ) ) && ( list[ i ].length > 0 ) )
        {
            if ( !( this.listContains( this.m_blacklist, list[ i ] ) ) )
            {
                result.push( list[ i ] );
            }
        }
    }
    return result;
}

/*
 * Check, whether entry is contained in the array list.
 */
StringComparer.prototype.listContains = function( list, entry )
{
    for ( var i = 0; i < list.length; i++ )
    {
        if ( list[ i ] == entry )
        {
            return true;
        }
    }
    return false;
}