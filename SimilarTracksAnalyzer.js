/*
*    Copyright (C) 2010 by Martin Höher <martin@rpdev.net>
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

Importer.loadQtBinding( "qt.core" );
Importer.loadQtBinding( "qt.gui" );
Importer.loadQtBinding( "qt.uitools" );

function SimilarTracksAnalyzer( plugin )
{
    this.m_plugin = plugin;

    // Load UI
    var uiLoader = new QUiLoader( this );
    var uifile = new QFile( Amarok.Info.scriptPath() + "/selectsimilarityform.ui" );
    uifile.open( QIODevice.ReadOnly );
    this.m_widget = uiLoader.load( uifile, this );
    uifile.close();

    this.m_widget.progressBar.hide();

    this.m_widget.yesLabel[ 'linkActivated(QString)' ].connect( this, this.linkClicked );
    this.m_widget.noLabel[ 'linkActivated(QString)' ].connect( this, this.linkClicked );
    this.m_widget.nextLabel[ 'linkActivated(QString)' ].connect( this, this.linkClicked );

    this.m_widget.startButton[ 'clicked()' ].connect( this, this.start );
    this.m_widget.stopButton[ 'clicked()' ].connect( this, this.stop );

    this.m_tracks = new Array();
    this.m_track1 = null;
    this.m_track2 = null;

    this.m_weights = null;
    this.m_breakOff = 0.0;
    this.m_currentValue = 0;

    this.m_running = 0;
    this.m_maxTime = 60000; // run about a minute. Will be longer depending on
                            // the underlying platform, but don't care, as the
                            // algo is usually run only once
    
    this.m_timer = new QTimer( this );
    this.m_timer.interval = 10;
    this.m_timer[ 'timeout()' ].connect( this, this.step );
}

SimilarTracksAnalyzer.prototype.show = function()
{
    if ( this.m_running == 0 )
    {
        this.m_tracks = new Array();
    }
    this.update();
    this.m_widget.show();
}

SimilarTracksAnalyzer.prototype.update = function()
{
    this.m_track1 = this.m_plugin.collection.randomTrack();
    this.m_track2 = this.m_plugin.collection.randomTrack();

    if ( this.m_track1 && this.m_track2 )
    {
         this.m_widget.track1.setText( this.m_track1.prettyName() );
         this.m_widget.track2.setText( this.m_track2.prettyName() );
    }
}

SimilarTracksAnalyzer.prototype.linkClicked = function( url )
{
    if ( url == "yes" )
    {
        this.m_tracks.push( new Array( this.m_track1, this.m_track2, true ) );
        this.m_plugin.playlist.addSimilarArtists( this.m_track1.artist(), this.m_track2.artist() );
        this.m_plugin.playlist.addSimilarGenres( this.m_track1.genre(), this.m_track2.genre() );
    } else if ( url == "no" )
    {
        this.m_tracks.push( new Array( this.m_track1, this.m_track2, false ) );
        this.m_plugin.playlist.decSimilarArtists( this.m_track1.artist(), this.m_track2.artist() );
        this.m_plugin.playlist.decSimilarGenres( this.m_track1.genre(), this.m_track2.genre() );
    }
    this.update();
}

SimilarTracksAnalyzer.prototype.start = function()
{
    this.m_widget.yesLabel.enabled = false;
    this.m_widget.noLabel.enabled = false;
    this.m_widget.nextLabel.enabled = false;
    this.m_widget.startButton.enabled = false;
    this.m_widget.progressBar.show();

    this.m_weights = this.m_plugin.configuration.getWeights();
    this.m_breakOff = this.m_plugin.configuration.minimalSimilarity();

    this.m_widget.progressBar.minimum = 0;
    this.m_widget.progressBar.maximum = this.m_tracks.length;
    this.m_widget.progressBar.value = 0;
    this.m_widget.progressBar.format = qsTranslate( "similartracksanalizer", "Comparing Tracks - %p%" );
    this.compareTracks();
    this.m_currentValue = this.testWeights( this.m_weights, this.m_breakOff );

    this.m_widget.progressBar.minimum = 0;
    this.m_widget.progressBar.maximum = this.m_maxTime;
    this.m_widget.progressBar.value = 0;
    this.m_widget.progressBar.format = qsTranslate( "similartracksanalizer", "Generating Weights - %p%" );
    
    this.m_timer.start();
    this.m_running = this.m_maxTime;
}

SimilarTracksAnalyzer.prototype.stop = function()
{
    this.m_tracks = new Array();
    this.m_running = 0;
    this.m_timer.stop();
    
    this.m_widget.yesLabel.enabled = true;
    this.m_widget.noLabel.enabled = true;
    this.m_widget.nextLabel.enabled = true;
    this.m_widget.startButton.enabled = true;

    this.m_widget.progressBar.hide();
    this.m_widget.progressBar.minimum = 0;
    this.m_widget.progressBar.maximum = 0;
    this.m_widget.progressBar.value = 0;
    this.m_widget.progressBar.format = "%p%";
}

SimilarTracksAnalyzer.prototype.compareTracks = function()
{
    for ( var i = 0; i < this.m_tracks.length; i++ )
    {
        this.m_tracks[ i ][ 3 ] = this.m_plugin.solver.compareTracks( this.m_tracks[ i ][ 0 ],
                                                                      this.m_tracks[ i ][ 1 ],
                                                                      true );
        this.m_widget.progressBar.value += 1;
    }
}

SimilarTracksAnalyzer.prototype.generateWeights = function()
{
    var result = new Array();
    for ( var i = 0; i < this.m_weights.length; i++ )
    {
        result.push( new Array( this.m_weights[ i ][ 0 ],
                                this.m_weights[ i ][ 1 ],
                                this.m_weights[ i ][ 2 ] ) );
    }
    
    var idx = parseInt( Math.random() * result.length, 10 );
    var w = result[ idx ][ 2 ];
    w += ( Math.random() * 0.2 - 0.1 );
    w = Math.max( 0, Math.min( w, 1 ) );
    result[ idx ][ 2 ] = w;

    return result;
}

SimilarTracksAnalyzer.prototype.testWeights = function( weights, breakOff )
{
    var matches = 0;
    
    for ( var i = 0; i < this.m_tracks.length; i++ )
    {
        for ( var j = 0; j < weights.length; j++ )
        {
            this.m_tracks[ i ][ 3 ].resetComponentWeight( weights[ j ][ 0 ], weights[ j ][ 2 ] );
        }
        var wouldBeSelected = this.m_tracks[ i ][ 3 ].similarity() > breakOff;
        if ( ( wouldBeSelected && this.m_tracks[ i ][ 2 ] ) ||
             ( !( wouldBeSelected ) && !( this.m_tracks[ i ][ 2 ] ) ) )
        {
            matches++;
        } else
        {
            matches--;
        }
    }

    return matches;
}

SimilarTracksAnalyzer.prototype.step = function()
{
    try
    {
        if ( this.m_running > 0 )
        {
            var newWeights = this.m_weights;
            var newBreakOff = this.m_breakOff;

            var alter = parseInt( Math.random() * ( this.m_weights.length + 1), 10 );

            switch ( alter )
            {
            case 0:
            {
                newBreakOff += ( Math.random() * 0.2 - 0.1 );
                newBreakOff = Math.max( 0, Math.min( newBreakOff, 1 ) );
            }

            default:
            {
                newWeights = this.generateWeights( this.m_weights );
            }

            }


            var newMatches = this.testWeights( newWeights, newBreakOff );

            if ( ( newMatches >= this.m_currentValue ) ||
                ( Math.random() <= Math.exp( - ( this.m_running / ( ( this.m_maxTime / 10 ) * 2 ) ) ) ) )
            {
                this.m_weights = newWeights;
                this.m_breakOff = newBreakOff;
                this.m_currentValue = newMatches;
            }
        }

        this.m_running -= 10;
        if ( this.m_running < 0 )
        {
            this.m_running = 0;
        }
        this.m_widget.progressBar.value = this.m_maxTime - this.m_running;

        if ( this.m_running == 0 )
        {
            this.m_timer.stop();
            var result = qsTranslate( "similartracksanalizer", "<b>Result</b><br/><b>Minimal Similarity:</b> %1<br/>" ).arg( this.m_breakOff );
            for ( var i = 0; i < this.m_weights.length; i++ )
            {
                result += "<br/>";
                result += "<b>" + this.m_weights[ i ][ 1 ] + ":</b> " + this.m_weights[ i ][ 2 ];
            }
            if ( Amarok.alert( result + "<br/><br/>" + qsTranslate( "similartracksanalizer", "Apply these values?" ), "questionYesNo" ) == 3 )
            {
                this.m_plugin.configuration.setWeights( this.m_weights );
                this.m_plugin.configuration.setMinimalSimilarity( this.m_breakOff );
            }
            this.stop();
        }
    } catch ( ex )
    {
        this.stop();
        printStackTrace( ex );
    }
}

