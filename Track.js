/*
 *    Copyright (C) 2009, 2010 by Martin Höher <martin@rpdev.net>    
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Class representing a single track.
 * This re-builds the Amarok-internal Track class, as we cannot use it directly
 * i.e. the collection script interface does not provide enough information
 * or functionality.
 * Basically, this class holds the track's ID and provides methods for
 * querying further information. The query results are cached to prevent further
 * calls.
 *
 * TODO:
 * Caching the properties might be somehow... faster... than querying each time.
 * However, we should certainly clear out from time to time, or make sure
 * only a very limited number of Track objects exist at once.
 * Otherwise, the plugin will tend to hold huge parts of the complete collection
 * in memory, this is neither intelligent nor good in terms of performance.
 */
function Track( id ) 
{
    Amarok.debug( "Creating Track" );
    
    this.m_id = id;

    this.m_needQueryAttrs = true;
    
    this.m_url      = null;
    this.m_uniqueid = null; //This can be used instead of a fully qualified URL.
                            // Should be more stable than reverse-engineering the
                            // complete path (esp. since the URL format that is
                            // allowed in Amarok.Playlist.addMedia() seems to
                            // change from time to time).
    this.m_track    = null;
    this.m_artist   = null;
    this.m_album    = null;
    this.m_composer = null;
    this.m_genre    = null;
    this.m_year     = null;
    this.m_score    = null;
    this.m_rating   = null;

    this.m_labels   = null;
}


Track.prototype.fillData = function( url, track, artist, album, composer, genre, year, score, rating )
{
    this.m_id       = -1;
    this.m_url      = new QUrl( url );
    this.m_uniqueid = null;
    this.m_track    = track;
    this.m_artist   = artist;
    this.m_album    = album;
    this.m_composer = composer;
    this.m_genre    = genre;
    this.m_year     = parseInt( year, 10 );
    this.m_score    = parseInt( score, 10 );
    this.m_rating   = parseInt( rating, 10 );

    this.m_needQueryAttrs = false;

    // this is just a try to enable further querying. If it fails... hey, it's OK
    var q = Amarok.Collection.query( "SELECT t.id " +
                                     " FROM tracks t " +
                                     " INNER JOIN urls u ON t.url = u.id " +
                                     " WHERE u.rpath like '%" + Amarok.Collection.escape( "" + this.toFileName() ) + "%'; " );
    if ( q.length == 1 ) // hey, found! Use the id (and hope it's referencing to what we hope
    {
        if ( !( isNaN( parseInt( q[ 0 ] ) ) ) )
        {
            this.m_id = parseInt( q[ 0 ] );
        }
    }
}

Track.prototype.id = function()
{
    return this.m_id;
}

Track.prototype.url = function()
{
    this.queryAttributes();
    return this.m_url;
}

Track.prototype.uniqueid = function()
{
    this.queryAttributes();
    return this.m_uniqueid;
}

Track.prototype.track = function()
{
    this.queryAttributes();
    return this.m_track;
}

Track.prototype.artist = function()
{
    this.queryAttributes();
    return this.m_artist;
}

Track.prototype.album = function()
{
    this.queryAttributes();
    return this.m_album;
}

Track.prototype.composer = function()
{
    this.queryAttributes();
    return this.m_composer;
}

Track.prototype.genre = function()
{
    this.queryAttributes();
    return this.m_genre;
}

Track.prototype.year = function()
{
    this.queryAttributes();
    return this.m_year;
}

Track.prototype.score = function()
{
    this.queryAttributes();
    return this.m_score;
}

Track.prototype.rating = function()
{
    this.queryAttributes();
    return this.m_rating;
}

Track.prototype.labels = function()
{
    if ( this.m_labels == null )
    {
        if ( this.m_id < 0 )
        {
            this.m_labels = new Array();
        } else
        {
            // fetch labels from DB
            var q = Amarok.Collection.query( "SELECT l.label " +
                                            " FROM tracks t " +
                                            " INNER JOIN urls u ON t.url = u.id " +
                                            " INNER JOIN urls_labels ul ON u.id = ul.url " +
                                            " INNER JOIN labels l ON ul.label = l.id " +
                                            " WHERE t.id = " + Amarok.Collection.escape( parseInt( this.m_id, 10 ) ) + ";" );
            this.m_labels = new Array();
            for ( var i = 0; i < q.length; i++ )
            {
                this.m_labels.push( q[ i ] );
            }
        }
    }
    return this.m_labels;
}

Track.prototype.prettyName = function()
{
    return this.track() + " on " + this.album() + " by " + this.artist();
}

Track.prototype.isLocalFile = function()
{
    return ( this.m_url.toString().indexOf( "file://" ) == 0 ) ||
           ( this.m_url.toString().indexOf( "./" ) == 0 );
}

Track.prototype.toFileName = function()
{
    var result = this.m_url.toString();
    result = result.replace( /^\./, "" ); //TODO: Check whether this is required.
    result = result.replace( /^file:\/\/\./, "" ); //TODO: Same here
    return result.replace( "file://", "" );
}

/*
 * Appends the track to the playlist.
 */
Track.prototype.appendToPlaylist = function()
{
    Amarok.debug( "Adding track " + this.url() + " to playlist..." );
    if ( this.m_uniqueid != null )
    {
        // Okay, we have the uniqueid, just hope Amarok handles it
        // correctly internally.
        Amarok.Playlist.addMedia( new QUrl( this.m_uniqueid ) );
    } else
    {
        // We do not have the uniqueid :( Just use the URL as
        // computed or get from somewhere else and hope
        // the formats used by Amarok and the script match.
        Amarok.Playlist.addMedia( new QUrl( this.url() ) );
    }
}

/*
 * Returns true, if the track is already in the playlist.
 */
Track.prototype.isInPlaylist = function()
{
    for ( var i = 0; i < Amarok.Playlist.totalTrackCount(); i++ )
    {
        var track = Amarok.Playlist.trackAt( i );
        if ( track.url == this.url() )
        {
            return true;
        }
    }
    return false;
}

/*
 * Retrieve information from the database.
 * This will try to fetch all information about the track
 * (which is identified by its id) in a single query.
 * Non-existent information will get null assigned.
 */
Track.prototype.queryAttributes = function()
{
    if ( this.m_needQueryAttrs )
    {
        this.m_needQueryAttrs = false;
        
        Amarok.debug( "Querying attributes for track " + this.m_id );
        var q = Amarok.Collection.query(
            "SELECT t.title, a.name, i.name, c.name, g.name, y.name, s.score, s.rating, u.rpath, u.uniqueid " +
            "FROM tracks AS t " +
            "LEFT OUTER JOIN albums AS a ON t.album = a.id " +
            "LEFT OUTER JOIN artists AS i ON t.artist = i.id " +
            "LEFT OUTER JOIN composers AS c ON t.composer = c.id " +
            "LEFT OUTER JOIN genres AS g ON t.genre = g.id " +
            "LEFT OUTER JOIN years AS y ON t.year = y.id " +
            "LEFT OUTER JOIN statistics AS s ON t.url = s.url " +
            "LEFT OUTER JOIN urls AS u ON t.url = u.id " +
            "WHERE t.id = " + Amarok.Collection.escape( parseInt( this.m_id, 10 ) ) + " " +
            ";"
        );
        this.m_track = q[ 0 ];
        this.m_album = q[ 1 ];
        this.m_artist = q[ 2 ];
        this.m_composer = q[ 3 ];
        this.m_genre = q[ 4 ];
        this.m_year = parseInt( q[ 5 ], 10 );
        this.m_score = parseInt( q[ 6 ], 10 );
        this.m_rating = parseInt ( q[ 7 ], 10 );

        var url = "" + q[ 8 ];
        url = url.replace( /\./, "" )
        this.m_url = new QUrl( "file://" + url );

        this.m_uniqueid = q[ 9 ];

        if ( isNaN( this.m_score ) )
        {
            this.m_score = 0;
        }
        if ( isNaN( this.m_rating ) )
        {
            this.m_rating = 0;
        }
    }
}
